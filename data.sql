insert into permission (naziv) values ('Adrese:view');
insert into permission (naziv) values ('Adrese:add');
insert into permission (naziv) values ('Adrese:edit');
insert into permission (naziv) values ('Adrese:delete');
insert into permission (naziv) values ('Adrese:search');
insert into permission (naziv) values ('Banke:view');
insert into permission (naziv) values ('Banke:add');
insert into permission (naziv) values ('Banke:edit');
insert into permission (naziv) values ('Banke:delete');
insert into permission (naziv) values ('Banke:search');
insert into permission (naziv) values ('DnevnaStanja:view');
insert into permission (naziv) values ('DnevnaStanja:add');
insert into permission (naziv) values ('DnevnaStanja:search');
insert into permission (naziv) values ('Drzave:view');
insert into permission (naziv) values ('Drzave:add');
insert into permission (naziv) values ('Drzave:edit');
insert into permission (naziv) values ('Drzave:delete');
insert into permission (naziv) values ('Drzave:search');
insert into permission (naziv) values ('NaloziZaPlacanje:view');
insert into permission (naziv) values ('NaloziZaPlacanje:add');
insert into permission (naziv) values ('NaloziZaPlacanje:edit');
insert into permission (naziv) values ('NaloziZaPlacanje:delete');
insert into permission (naziv) values ('NaloziZaPlacanje:search');
insert into permission (naziv) values ('NaseljenaMesta:view');
insert into permission (naziv) values ('NaseljenaMesta:add');
insert into permission (naziv) values ('NaseljenaMesta:edit');
insert into permission (naziv) values ('NaseljenaMesta:delete');
insert into permission (naziv) values ('NaseljenaMesta:search');
insert into permission (naziv) values ('PoslovneGodine:view');
insert into permission (naziv) values ('PoslovneGodine:add');
insert into permission (naziv) values ('PoslovneGodine:edit');
insert into permission (naziv) values ('PoslovneGodine:delete');
insert into permission (naziv) values ('PoslovneGodine:search');
insert into permission (naziv) values ('PoslovniPartner:view');
insert into permission (naziv) values ('PoslovniPartner:add');
insert into permission (naziv) values ('PoslovniPartner:edit');
insert into permission (naziv) values ('PoslovniPartner:delete');
insert into permission (naziv) values ('PoslovniPartner:search');
insert into permission (naziv) values ('PredloziZaPlacanje:view');
insert into permission (naziv) values ('PredloziZaPlacanje:add');
insert into permission (naziv) values ('PredloziZaPlacanje:edit');
insert into permission (naziv) values ('PredloziZaPlacanje:delete');
insert into permission (naziv) values ('PredloziZaPlacanje:search');
insert into permission (naziv) values ('Preduzeca:view');
insert into permission (naziv) values ('Preduzeca:add');
insert into permission (naziv) values ('Preduzeca:edit');
insert into permission (naziv) values ('Preduzeca:delete');
insert into permission (naziv) values ('Preduzeca:search');
insert into permission (naziv) values ('Racuni:view');
insert into permission (naziv) values ('Racuni:add');
insert into permission (naziv) values ('Racuni:edit');
insert into permission (naziv) values ('Racuni:delete');
insert into permission (naziv) values ('Racuni:search');
insert into permission (naziv) values ('RacuniPartnera:view');
insert into permission (naziv) values ('RacuniPartnera:add');
insert into permission (naziv) values ('RacuniPartnera:edit');
insert into permission (naziv) values ('RacuniPartnera:delete');
insert into permission (naziv) values ('RacuniPartnera:search');
insert into permission (naziv) values ('SifrarnikDelatnosti:view');
insert into permission (naziv) values ('SifrarnikDelatnosti:add');
insert into permission (naziv) values ('SifrarnikDelatnosti:edit');
insert into permission (naziv) values ('SifrarnikDelatnosti:delete');
insert into permission (naziv) values ('SifrarnikDelatnosti:search');
insert into permission (naziv) values ('StaSePlaca:view');
insert into permission (naziv) values ('StaSePlaca:add');
insert into permission (naziv) values ('StaSePlaca:edit');
insert into permission (naziv) values ('StaSePlaca:delete');
insert into permission (naziv) values ('StaSePlaca:search');
insert into permission (naziv) values ('StavkeIzvoda:view');
insert into permission (naziv) values ('StavkeIzvoda:search');
insert into permission (naziv) values ('Fakture:view');
insert into permission (naziv) values ('Fakture:edit');
insert into permission (naziv) values ('Fakture:search');
insert into permission (naziv) values ('ZatvaranjeIF:view');
insert into permission (naziv) values ('ZatvaranjeIF:add');
insert into permission (naziv) values ('ZatvaranjeIF:edit');
insert into permission (naziv) values ('ZatvaranjeIF:delete');
insert into permission (naziv) values ('ZatvaranjeIF:search');
insert into permission (naziv) values ('ZatvaranjeUF:view');
insert into permission (naziv) values ('ZatvaranjeUF:add');
insert into permission (naziv) values ('ZatvaranjeUF:edit');
insert into permission (naziv) values ('ZatvaranjeUF:delete');
insert into permission (naziv) values ('ZatvaranjeUF:search');
insert into permission (naziv) values ('Izvestaji:view');

insert into role (naziv) values ('admin');
insert into role (naziv) values ('manager');
insert into role (naziv) values ('bookkeeper');

select * from permission;

insert into role_permission (role_id, permission_id) values (1, 1);
insert into role_permission (role_id, permission_id) values (1, 2);
insert into role_permission (role_id, permission_id) values (1, 3);
insert into role_permission (role_id, permission_id) values (1, 4);
insert into role_permission (role_id, permission_id) values (1, 5);
insert into role_permission (role_id, permission_id) values (1, 6);
insert into role_permission (role_id, permission_id) values (1, 7);
insert into role_permission (role_id, permission_id) values (1, 8);
insert into role_permission (role_id, permission_id) values (1, 9);
insert into role_permission (role_id, permission_id) values (1, 10);
insert into role_permission (role_id, permission_id) values (3, 11);
insert into role_permission (role_id, permission_id) values (3, 12);
insert into role_permission (role_id, permission_id) values (3, 13);
insert into role_permission (role_id, permission_id) values (1, 14);
insert into role_permission (role_id, permission_id) values (1, 15);
insert into role_permission (role_id, permission_id) values (1, 16);
insert into role_permission (role_id, permission_id) values (1, 17);
insert into role_permission (role_id, permission_id) values (1, 18);
insert into role_permission (role_id, permission_id) values (3, 19);
insert into role_permission (role_id, permission_id) values (3, 20);
insert into role_permission (role_id, permission_id) values (3, 21);
insert into role_permission (role_id, permission_id) values (3, 22);
insert into role_permission (role_id, permission_id) values (3, 23);
insert into role_permission (role_id, permission_id) values (1, 24);
insert into role_permission (role_id, permission_id) values (1, 25);
insert into role_permission (role_id, permission_id) values (1, 26);
insert into role_permission (role_id, permission_id) values (1, 27);
insert into role_permission (role_id, permission_id) values (1, 28);
insert into role_permission (role_id, permission_id) values (3, 29);
insert into role_permission (role_id, permission_id) values (3, 30);
insert into role_permission (role_id, permission_id) values (3, 31);
insert into role_permission (role_id, permission_id) values (3, 32);
insert into role_permission (role_id, permission_id) values (3, 33);
insert into role_permission (role_id, permission_id) values (3, 34);
insert into role_permission (role_id, permission_id) values (3, 35);
insert into role_permission (role_id, permission_id) values (3, 36);
insert into role_permission (role_id, permission_id) values (3, 37);
insert into role_permission (role_id, permission_id) values (3, 38);
insert into role_permission (role_id, permission_id) values (3, 39);
insert into role_permission (role_id, permission_id) values (3, 40);
insert into role_permission (role_id, permission_id) values (3, 41);
insert into role_permission (role_id, permission_id) values (3, 42);
insert into role_permission (role_id, permission_id) values (3, 43);
insert into role_permission (role_id, permission_id) values (1, 44);
insert into role_permission (role_id, permission_id) values (1, 45);
insert into role_permission (role_id, permission_id) values (1, 46);
insert into role_permission (role_id, permission_id) values (1, 47);
insert into role_permission (role_id, permission_id) values (1, 48);
insert into role_permission (role_id, permission_id) values (3, 49);
insert into role_permission (role_id, permission_id) values (3, 50);
insert into role_permission (role_id, permission_id) values (3, 51);
insert into role_permission (role_id, permission_id) values (3, 52);
insert into role_permission (role_id, permission_id) values (3, 53);
insert into role_permission (role_id, permission_id) values (3, 54);
insert into role_permission (role_id, permission_id) values (3, 55);
insert into role_permission (role_id, permission_id) values (3, 56);
insert into role_permission (role_id, permission_id) values (3, 57);
insert into role_permission (role_id, permission_id) values (3, 58);
insert into role_permission (role_id, permission_id) values (1, 59);
insert into role_permission (role_id, permission_id) values (1, 60);
insert into role_permission (role_id, permission_id) values (1, 61);
insert into role_permission (role_id, permission_id) values (1, 62);
insert into role_permission (role_id, permission_id) values (1, 63);
insert into role_permission (role_id, permission_id) values (3, 64);
insert into role_permission (role_id, permission_id) values (3, 65);
insert into role_permission (role_id, permission_id) values (3, 66);
insert into role_permission (role_id, permission_id) values (3, 67);
insert into role_permission (role_id, permission_id) values (3, 68);
insert into role_permission (role_id, permission_id) values (3, 69);
insert into role_permission (role_id, permission_id) values (3, 70);
insert into role_permission (role_id, permission_id) values (3, 71);
insert into role_permission (role_id, permission_id) values (3, 72);
insert into role_permission (role_id, permission_id) values (3, 73);
insert into role_permission (role_id, permission_id) values (3, 74);
insert into role_permission (role_id, permission_id) values (3, 75);
insert into role_permission (role_id, permission_id) values (3, 76);
insert into role_permission (role_id, permission_id) values (3, 77);
insert into role_permission (role_id, permission_id) values (3, 78);
insert into role_permission (role_id, permission_id) values (3, 79);
insert into role_permission (role_id, permission_id) values (3, 80);
insert into role_permission (role_id, permission_id) values (3, 81);
insert into role_permission (role_id, permission_id) values (3, 82);
insert into role_permission (role_id, permission_id) values (3, 83);
insert into role_permission (role_id, permission_id) values (3, 84);

-- DRZAVE--

INSERT INTO `jdbc14`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('1', 'Srbija', 'SRB');
INSERT INTO `jdbc14`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('2', 'Crna Gora', 'MNE');
INSERT INTO `jdbc14`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('3', 'Bosna i Hercegovina', 'BIH');
INSERT INTO `jdbc14`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('4', 'Hrvatska', 'CRO');
INSERT INTO `jdbc14`.`drzava` (`id`, `naziv`, `oznaka`) VALUES ('5', 'Slovenija', 'SLO');

-- NASELJENA MESTA--

INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('1', 'Novi Sad', '21000', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('2', 'Beograd', '110000', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('3', 'Asanja', '22418', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('4', 'Babusnica', '18330', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('5', 'Vilovo', '21246', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('6', 'Zajecar', '19000', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('7', 'Kragujevac', '34000', '1');
INSERT INTO `jdbc14`.`naseljeno_mesto` (`id`, `naziv`, `post_broj`, `drzava`) VALUES ('8', 'Kraljevo', '36000', '1');


-- ADRESE--

INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('1', '18', 'Partizanskih baza', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('2', '25', 'Vladike Cirica', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('3', '9', 'Bulevar Jovana Ducica', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('4', '65', 'Cara Lazara', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('5', '1', 'Branislava Borote', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('6', '4', 'Bulevar Kneza Milosa', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('7', '56', 'Bulevar Slobodana Jovanovica', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('8', '41', 'Bate Brkica', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('9', '33', 'Balzakova', '1');
INSERT INTO `jdbc14`.`adresa` (`id`, `broj`, `ulica`, `nas_mesto`) VALUES ('10', '14', 'Stevana Hristica', '1');


-- sifrarnik delatnosti-- 

INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('1', 'Proizvodnja prehrambenih proizvoda', '10');
INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('2', '	Prerada i konzervisanje mesa i proizvoda od mesa', '101');
INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('3', '	Proizvodnja sladoleda', '1052');
INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('4', '	Proizvodnja ostale odeće', '1413');
INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('5', '	Popravka ostale opreme', '3319');
INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('6', '	Trgovina na malo hranom, pićima i duvanom u specijalizovanim prodavnicama', '472');
INSERT INTO `jdbc14`.`sifrarnik_delatnosti` (`id`, `naziv_delatnosti`, `sifra`) VALUES ('7', 'Trgovina na malo računarima, perifernim jedinicama i softverom u specijalizovanim ', '4741');


-- preduzece--

INSERT INTO `jdbc14`.`preduzece` (`id`, `br_telefona`, `email`, `fax`, `maticni_broj`, `naziv`, `pib`, `adresa`, `sifra_delatnosti`) VALUES ('1', '021456987', 'preduzece1@gmail.com', '021456987', '98745632', 'Preduzece 1', '963258741', '1', '1');
INSERT INTO `jdbc14`.`preduzece` (`id`, `br_telefona`, `email`, `fax`, `maticni_broj`, `naziv`, `pib`, `adresa`, `sifra_delatnosti`) VALUES ('2', '021987455', 'preduzece2@gmail.com', '021963258', '14785236', 'Preduzece2', '321456987', '2', '2');


-- poslovni partneri--

INSERT INTO `jdbc14`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('1', '021789954', 'pp1@gmail.com', '18745632', 'pp1', '999456320', 'prodaje', '2', '1', '2');
INSERT INTO `jdbc14`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('2', '021789654', 'pp2@gmail.com', '98745632', 'pp2', '987456320', 'prodaje', '3', '1', '2');
INSERT INTO `jdbc14`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('3', '021654789', 'pp3@gmail.com', '99999999', 'pp3', '888888888', 'kupuje', '4', '1', '3');
INSERT INTO `jdbc14`.`poslovni_partner` (`id`, `br_telefona`, `email`, `maticni_broj`, `naziv`, `pib`, `vrsta`, `adresa`, `preduzece`, `sifra_delatnosti`) VALUES ('4', '021659999', 'pp4@gmail.com', '88899777', 'pp4', '999999999', 'kupuje', '5', '1', '3');


-- poslovna godina 

INSERT INTO `jdbc14`.`poslovna_godina` (`id`, `godina`, `zakljucena`, `preduzece`) VALUES ('1', '2015', true, '1');
INSERT INTO `jdbc14`.`poslovna_godina` (`id`, `godina`, `zakljucena`, `preduzece`) VALUES ('2', '2016', true, '1');
INSERT INTO `jdbc14`.`poslovna_godina` (`id`, `godina`, `zakljucena`, `preduzece`) VALUES ('3', '2017', false, '1');

-- banka

INSERT INTO `jdbc14`.`banka` (`id`, `naziv`) VALUES ('1', 'NLB');
INSERT INTO `jdbc14`.`banka` (`id`, `naziv`) VALUES ('2', 'Intesa');
INSERT INTO `jdbc14`.`banka` (`id`, `naziv`) VALUES ('3', 'Erste');

-- racun

INSERT INTO `jdbc14`.`racun` (`id`, `br_racuna`, `banka`, `preduzece`) VALUES ('1', '123654789698547896', '1', '1');
INSERT INTO `jdbc14`.`racun` (`id`, `br_racuna`, `banka`, `preduzece`) VALUES ('2', '123963258741114569', '1', '1');

-- racun partnera

INSERT INTO `jdbc14`.`racun_partnera` (`id`, `br_racuna`, `banka`, `poslovni_partner`) VALUES ('1', '456987412563258741', '2', '1');
INSERT INTO `jdbc14`.`racun_partnera` (`id`, `br_racuna`, `banka`, `poslovni_partner`) VALUES ('2', '456987456321023658', '2', '1');
INSERT INTO `jdbc14`.`racun_partnera` (`id`, `br_racuna`, `banka`, `poslovni_partner`) VALUES ('3', '123147852693003500', '1', '2');
INSERT INTO `jdbc14`.`racun_partnera` (`id`, `br_racuna`, `banka`, `poslovni_partner`) VALUES ('4', '123147936589752855', '1', '3');
INSERT INTO `jdbc14`.`racun_partnera` (`id`, `br_racuna`, `banka`, `poslovni_partner`) VALUES ('5', '456987456300178899', '2', '4');

-- fakutra 

INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('1', '1', '2017.01.03.', '2017.01.14.', 'rsd', '10000.00', '10000.00', '2000.00', '0.00', '8000.00', '8000.00', '0.00', 'ulazne', '3', '1', '1', '1');
INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('2', '2', '2017.01.09.', '2017.01.15.', 'rsd', '25500.00', '25500.00', '5100.00', '0.00', '20400.00', '20400.00', '0.00', 'ulazne', '3', '1', '1', '1');
INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('3', '22', '2017.02.13.', '2017.02.15.', 'rsd', '8000.00', '8000.00', '1600.00', '0.00', '6400.00', '6400.00', '0.00', 'ulazne', '3', '2', '1', '3');
INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('4', '3', '2017.02.25.', '2017.02.28.', 'rsd', '10000.00', '10000.00', '2000.00', '0.00', '8000.00', '8000.00', '0.00', 'ulazne', '3', '2', '1', '3');
INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('5', '6', '2017.02.28.', '2017.03.10.', 'rsd', '14000.00', '24000.00', '2800.00', '0.00', '11200.00', '11200.00', '0.00', 'izlazne', '3', '3', '1', '4');
INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('6', '9', '2017.01.02.', '2017.01.15.', 'rsd', '18000.00', '18000.00', '3600.00', '900.00', '14400.00', '12000.00', '2400.00', 'izlazne', '3', '3', '1', '4');
INSERT INTO `jdbc14`.`faktura` (`id`, `broj_fakture`, `datum_fakture`, `datum_valute`, `oznaka_valute`, `preostali_iznos`, `uk_iznos`, `ukupan_porez`, `ukupan_rabat`, `ukupno_robaiusluge`, `vrednost_robe`, `vrednost_usluga`, `vrsta_fakture`, `poslovna_godina`, `poslovni_partner`, `preduzece`, `racun_partnera`) VALUES ('7', '23', '2017.03.08.', '2017.03.20.', 'rsd', '35000.00', '35000.00', '7000.00', '1750.00', '28000.00', '27000.00', '1000.00', 'izlazne', '3', '4', '1', '5');

-- stavke fakture

INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('1', '0.00', 'kg', '100.00', '10', 'Brasno', '0.00', '1', '200.00', '0.00', '1000.00', '1');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('2', '0.00', 'kg', '50.00', '20', 'Secer', '0.00', '2', '200.00', '0.00', '1000.00', '1');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('3', '0.00', 'l', '100.00', '50', 'Mleko', '0.00', '3', '1000.00', '0.00', '5000.00', '1');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('4', '0.00', 'l', '100.00', '30', 'Jogurt', '0.00', '4', '600.00', '0.00', '3000.00', '1');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('5', '0.00', 'kom', '15000.00', '1', 'Ogrlica', '0.00', '1', '3000.00', '0.00', '15000.00', '2');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('6', '0.00', 'kom', '5000.00', '2', 'Narukvica', '0.00', '2', '2000.00', '0.00', '10000.00', '2');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('7', '0.00', 'kom', '500.00', '1', 'Mindjuse', '0.00', '3', '100.00', '0.00', '500.00', '2');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('8', '0.00', 'kom', '800.00', '5', 'Knjiga', '0.00', '1', '800.00', '0.00', '4000.00', '3');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('9', '0.00', 'kom', '800.00', '5', 'Knjiga', '0.00', '2', '800.00', '0.00', '4000.00', '3');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('10', '0.00', 'kg', '100.00', '10', 'Brasno', '0.00', '1', '200.00', '0.00', '1000.00', '4');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('11', '0.00', 'kg', '50.00', '20', 'Secer', '0.00', '2', '200.00', '0.00', '1000.00', '4');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('12', '0.00', 'l', '100.00', '50', 'Mleko', '0.00', '3', '1000.00', '0.00', '5000.00', '4');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('13', '0.00', 'l', '100.00', '30', 'Jogurt', '0.00', '4', '600.00', '0.00', '3000.00', '4');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('14', '0.00', 'kom', '1000.00', '14', 'Knjiga', '0.00', '1', '2800.00', '0.00', '14000.00', '5');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('15', '400.00', 'kg', '1000.00', '8', 'Torta', '0.05', '1', '1600.00', '8000.00', '8400.00', '6');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('16', '300.00', 'kg', '600.00', '10', 'Sitni kolaci', '0.05', '2', '1200.00', '6000.00', '6300.00', '6');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('17', '200.00', 'kom', '200.00', '20', 'Mafini', '0.05', '3', '800.00', '4000.00', '4200.00', '6');
INSERT INTO `jdbc14`.`stavka_fakture` (`id`, `iznos_rabata`, `jedinica_mere`, `jedinicna_cena`, `kolicina`, `naziv_robe_usluge`, `procenat_rabata`, `redni_broj`, `ukupan_porez`, `umanjeno_za_rabat`, `vrednost`, `stavka_fakture`) VALUES ('18', '1750.00', 'set', '36750.00', '1', 'Set nakit', '0.05', '1', '7000.00', '35000.00', '36750.00', '7');

-- dnevno stanje

INSERT INTO `jdbc14`.`dnevno_stanje` (`id`, `datum_izvoda`, `novo_stanje`, `prethodno_stanje`, `rezervisano`, `stanje_na_teret`, `stanjeukorist`, `racun`) VALUES ('1', '2017.01.10.', '121000', '129000', '0', '8000', '0', '1');
INSERT INTO `jdbc14`.`dnevno_stanje` (`id`, `datum_izvoda`, `novo_stanje`, `prethodno_stanje`, `rezervisano`, `stanje_na_teret`, `stanjeukorist`, `racun`) VALUES ('2', '2017.01.13.', '137000', '121000', '0', '2000', '18000', '1');

-- stavke izvoda

INSERT INTO `jdbc14`.`stavka_izvoda` (`id`, `datum_naloga`, `datum_valute`, `duznik`, `iznos`, `model_odobrenja`, `model_zaduzenja`, `poziv_na_broj_odobrenja`, `poziv_na_broj_zaduzenja`, `preostali_iznos`, `primalac`, `racun_duznika`, `racun_primaoca`, `smer`, `svrha_placanja`, `dnevno_stanje`) VALUES ('1', '2017.01.09.', '2017.01.10.', 'Preduzece 1', '8000.00', '97', '97', '1', '1', '8000', 'pp1', '123654789698547896', '456987412563258741', 'D', 'Faktura 1', '1');
INSERT INTO `jdbc14`.`stavka_izvoda` (`id`, `datum_naloga`, `datum_valute`, `duznik`, `iznos`, `model_odobrenja`, `model_zaduzenja`, `poziv_na_broj_odobrenja`, `poziv_na_broj_zaduzenja`, `preostali_iznos`, `primalac`, `racun_duznika`, `racun_primaoca`, `smer`, `svrha_placanja`, `dnevno_stanje`) VALUES ('2', '2017.01.11.', '2017.01.13.', 'Preduzece 1', '2000', '97', '97', '21252', '21252', '2000', 'pp1', '123654789698547896', '456987412563258741', 'D', 'Faktura 1', '2');
INSERT INTO `jdbc14`.`stavka_izvoda` (`id`, `datum_naloga`, `datum_valute`, `duznik`, `iznos`, `model_odobrenja`, `model_zaduzenja`, `poziv_na_broj_odobrenja`, `poziv_na_broj_zaduzenja`, `preostali_iznos`, `primalac`, `racun_duznika`, `racun_primaoca`, `smer`, `svrha_placanja`, `dnevno_stanje`) VALUES ('3', '2017.01.11.', '2017.01.13.', 'pp3', '18000', '97', '97', '14564136', '4654165', '18000', 'Preduzece 1', '123147936589752855', '123654789698547896', 'P', 'Faktura 6', '2');

