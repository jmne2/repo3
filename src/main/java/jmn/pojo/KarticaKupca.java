package jmn.pojo;

import java.sql.Date;

public class KarticaKupca {

	private Date datumDonji;
	
	private Date datumGornji;
	
	private String kupacPIB;

	public KarticaKupca() {
	}

	public KarticaKupca(Date datumDonji, Date datumGornji, String kupacPIB) {
		super();
		this.datumDonji = datumDonji;
		this.datumGornji = datumGornji;
		this.kupacPIB = kupacPIB;
	}

	public Date getDatumDonji() {
		return datumDonji;
	}

	public void setDatumDonji(Date datumDonji) {
		this.datumDonji = datumDonji;
	}

	public Date getDatumGornji() {
		return datumGornji;
	}

	public void setDatumGornji(Date datumGornji) {
		this.datumGornji = datumGornji;
	}

	public String getKupacPIB() {
		return kupacPIB;
	}

	public void setKupacPIB(String kupacPIB) {
		this.kupacPIB = kupacPIB;
	}
	
	
}
