package jmn.pojo;

import java.sql.Date;

public class Datumi {

	private Date datumDonji;
	private Date datumGornji;
	
	
	public Datumi() {
	}


	public Datumi(Date datumDonji, Date datumGornji) {
		super();
		this.datumDonji = datumDonji;
		this.datumGornji = datumGornji;
	}


	public Date getDatumDonji() {
		return datumDonji;
	}


	public void setDatumDonji(Date datumDonji) {
		this.datumDonji = datumDonji;
	}


	public Date getDatumGornji() {
		return datumGornji;
	}


	public void setDatumGornji(Date datumGornji) {
		this.datumGornji = datumGornji;
	}
	
	
}
