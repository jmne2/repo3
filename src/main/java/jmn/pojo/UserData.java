
package jmn.pojo;

import java.util.HashSet;
import java.util.Set;

import jmn.models.Preduzece;
import jmn.models.Role;

public class UserData {

	private Long id;
	
	private String uname;
	
	private Set<Role> roles = new HashSet<Role>();
	
	private Preduzece preduzece;

	public UserData(Long id, String uname, Set<Role> roles, Preduzece preduzece) {
		super();
		this.id = id;
		this.uname = uname;
		this.roles = roles;
		this.preduzece = preduzece;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}

	public Long getId() {
		return id;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}
}

