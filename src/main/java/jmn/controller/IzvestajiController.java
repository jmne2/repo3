package jmn.controller;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jmn.annotation.PermissionType;
import jmn.models.Faktura;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.pojo.Datumi;
import jmn.pojo.KarticaKupca;
import jmn.pojo.UserData;
import jmn.services.PoslovniPartnerService;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Controller
public class IzvestajiController {
	
	private Connection conn = null;
	
	@Autowired 
	private PoslovniPartnerService pps;
	
	
	
	@PermissionType("Izvestaji:view")
	@RequestMapping(value="/izvestaji/KUF/", method=RequestMethod.POST, produces = "application/pdf")
	public ResponseEntity<Void> fetchKUF(HttpSession session, @RequestBody Datumi datumi) throws SQLException {
		UserData u = (UserData)session.getAttribute("user");
		Preduzece preduzece = u.getPreduzece(); 
		System.out.println("datum donji: " + datumi.getDatumDonji());
		System.out.println("datum gornji: "+ datumi.getDatumGornji());
		
	//    java.sql.Date sqlDateD = new java.sql.Date(datumDonji);
	//    java.sql.Date sqlDateG = new java.sql.Date(datumGornji.getTime());
		try {
    		
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc14?" + "user=root&password=admin123");
	  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Pib preduzeca je: " +preduzece.getPib());
		System.out.println("datum donji: " + datumi.getDatumDonji());
		System.out.println("datum gornji: "+ datumi.getDatumGornji());
		System.out.println("konekcija je : "+ conn.getCatalog().length());
		try {
			 String status = "P";  // i sve vrste
				Map params = new HashMap();
				params.put("preduzece",String.valueOf(preduzece.getPib()));
				params.put("datumDonji", datumi.getDatumDonji());
				params.put("datumGornji", datumi.getDatumGornji());
		
			System.out.println(getClass().getResource("KUF.jasper"));
			JasperPrint jp = JasperFillManager.fillReport(
			getClass().getResource("KUF.jasper").openStream(),
			params, conn);
			
		//	String htmlpath="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.jsp";
			String filename="src\\main\\webapp\\Reports\\KUF.pdf";
		//	JasperExportManager.exportReportToHtmlFile(jp,htmlpath);
			JasperExportManager.exportReportToPdfFile(jp, filename);
			 
			
		conn.close();
			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		fetchKUFPDF("KUF.pdf");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}  
	
	
	@PermissionType("Izvestaji:view")
	@RequestMapping(value="/izvestaji/KIF/", method=RequestMethod.POST, produces = "application/pdf")
	public ResponseEntity<Void> fetchKIF(HttpSession session, @RequestBody Datumi datumi) throws SQLException {
		UserData u = (UserData)session.getAttribute("user");
		Preduzece preduzece = u.getPreduzece(); 
		System.out.println("datum donji: " + datumi.getDatumDonji());
		System.out.println("datum gornji: "+ datumi.getDatumGornji());
		
	//    java.sql.Date sqlDateD = new java.sql.Date(datumDonji);
	//    java.sql.Date sqlDateG = new java.sql.Date(datumGornji.getTime());
		try {
    		
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc14?" + "user=root&password=admin123");
	  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Pib preduzeca je: " +preduzece.getPib());
		System.out.println("datum donji: " + datumi.getDatumDonji());
		System.out.println("datum gornji: "+ datumi.getDatumGornji());
		try {
			 String status = "P";  // i sve vrste
				Map params = new HashMap();
				params.put("preduzece",String.valueOf(preduzece.getPib()));
				params.put("datumDonji", datumi.getDatumDonji());
				params.put("datumGornji", datumi.getDatumGornji());
		
			System.out.println(getClass().getResource("KIF.jasper"));
			JasperPrint jp = JasperFillManager.fillReport(
			getClass().getResource("KIF.jasper").openStream(),
			params, conn);
			
		//	String htmlpath="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.jsp";
			String filename="src\\main\\webapp\\Reports\\KIF.pdf";
		//	JasperExportManager.exportReportToHtmlFile(jp,htmlpath);
			JasperExportManager.exportReportToPdfFile(jp, filename);
			 
			
		conn.close();
			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		fetchKUFPDF("KIF.pdf");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}  
	
	@PermissionType("Izvestaji:view")
	@RequestMapping(value="/izvestaji/partneri/", method=RequestMethod.POST, produces = "application/pdf")
	public ResponseEntity<Void> fetchKarticeKupca(HttpSession session, @RequestBody KarticaKupca kupci) throws SQLException {
		UserData u = (UserData)session.getAttribute("user");
		Preduzece preduzece = u.getPreduzece(); 
	
	//    java.sql.Date sqlDateD = new java.sql.Date(datumDonji);
	//    java.sql.Date sqlDateG = new java.sql.Date(datumGornji.getTime());
		try {
    		
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc14?" + "user=root&password=admin123");
	  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Pib preduzeca je: " +preduzece.getPib());
		System.out.println("datum donji: " + kupci.getDatumDonji());
		System.out.println("datum gornji: "+ kupci.getDatumGornji());
		System.out.println("PIB kupca je" + kupci.getKupacPIB());
		try {
			 String status = "P";  // i sve vrste
				Map params = new HashMap();
				params.put("preduzece",String.valueOf(preduzece.getPib()));
				params.put("datumDonji", kupci.getDatumDonji());
				params.put("datumGornji", kupci.getDatumGornji());
				params.put("kupac", kupci.getKupacPIB());
		
			System.out.println(getClass().getResource("KarticaKupaca.jasper"));
			JasperPrint jp = JasperFillManager.fillReport(
			getClass().getResource("KarticaKupaca.jasper").openStream(),
			params, conn);
			
		//	String htmlpath="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.jsp";
			String filename="src\\main\\webapp\\Reports\\KarticaKupaca.pdf";
		//	JasperExportManager.exportReportToHtmlFile(jp,htmlpath);
			JasperExportManager.exportReportToPdfFile(jp, filename);
			 
			
		conn.close();
			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		fetchKUFPDF("KarticaKupaca.pdf");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}  
	
	@PermissionType("Izvestaji:view")
	@RequestMapping(value="/izvestaji/IOS/", method=RequestMethod.POST, produces = "application/pdf")
	public ResponseEntity<Void> fetchIOS(HttpSession session, @RequestBody PoslovniPartner partner) throws SQLException {
		UserData u = (UserData)session.getAttribute("user");
		Preduzece preduzece = u.getPreduzece(); 
		
		try {
    		
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc14?" + "user=root&password=admin123");
	  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			 String status = "P";  // i sve vrste
				Map params = new HashMap();
				params.put("partner", partner.getPib());
				
			if(partner.getVrsta().equals("kupuje")){
				
				System.out.println(getClass().getResource("IOSKupac.jasper"));
				JasperPrint jp = JasperFillManager.fillReport(
				getClass().getResource("IOSKupac.jasper").openStream(),
				params, conn);
				
				String filename="src\\main\\webapp\\Reports\\IOSKupac.pdf";
				JasperExportManager.exportReportToPdfFile(jp, filename);
				
				fetchKUFPDF("IOSKupac.pdf");
			}
			else{
				System.out.println(getClass().getResource("IOSDobavljac.jasper"));
				JasperPrint jp = JasperFillManager.fillReport(
				getClass().getResource("IOSDobavljac.jasper").openStream(),
				params, conn);
				
				String filename="src\\main\\webapp\\Reports\\IOSDobavljac.pdf";
				JasperExportManager.exportReportToPdfFile(jp, filename);
				
				fetchKUFPDF("IOSDobavljac.pdf");
			}
			
			 
			
		conn.close();
			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
	
		return new ResponseEntity<Void>(HttpStatus.OK);
	}  
	
	@PermissionType("Izvestaji:view")
	@RequestMapping(value="/izvestaji/sviPartneri/", method=RequestMethod.GET)
	public ResponseEntity<List<PoslovniPartner>> fetchSviPartneri(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");

		Preduzece preduzece = u.getPreduzece(); 
		
		List<PoslovniPartner> partneri = pps.findByPreduzece(preduzece);
		if(partneri == null)
			return new ResponseEntity<List<PoslovniPartner>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<PoslovniPartner>>(partneri, HttpStatus.OK);
	}
	
	@PermissionType("Izvestaji:view")
	@RequestMapping(value="/izvestaji/kupci/", method=RequestMethod.GET)
	public ResponseEntity<List<PoslovniPartner>> fetchKupce(HttpSession session){
		UserData u = (UserData)session.getAttribute("user");

		Preduzece preduzece = u.getPreduzece(); 
		
		List<PoslovniPartner> partneri = pps.findByPreduzeceAndVrsta(preduzece, "kupuje");
		if(partneri == null)
			return new ResponseEntity<List<PoslovniPartner>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<PoslovniPartner>>(partneri, HttpStatus.OK);
	}
	
	public  final void  fetchKUFPDF(String parametar){
/*		 String filename="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.pdf";
		File file = new File(filename);
		    byte[] document = FileCopyUtils.copyToByteArray(file);
		    HttpHeaders header = new HttpHeaders();
		    header.setContentType(new MediaType("application", "pdf"));
		    header.set("Content-Disposition", "inline; filename=" + file.getName());
		    header.setContentLength(document.length);
		    return new HttpEntity<byte[]>(document, header);
		*/
		 try {
			 String filename="src\\main\\webapp\\Reports\\"+parametar;
				if ((new File(filename)).exists()) {
					if(parametar.contains("KUF")){
						Process p = Runtime
						   .getRuntime()
						   .exec("rundll32 url.dll,FileProtocolHandler E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\Reports\\KUF.pdf");
						p.waitFor();
					}else if(parametar.contains("KIF")){
						Process p = Runtime
								   .getRuntime()
								   .exec("rundll32 url.dll,FileProtocolHandler E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\Reports\\KIF.pdf");
								p.waitFor();
					}else if(parametar.contains("KarticaKupaca")){
						Process p = Runtime
						   .getRuntime()
						   .exec("rundll32 url.dll,FileProtocolHandler E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\Reports\\KarticaKupaca.pdf");
						p.waitFor();
					}else if(parametar.contains("IOSKupac")){
						Process p = Runtime
								   .getRuntime()
								   .exec("rundll32 url.dll,FileProtocolHandler E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\Reports\\IOSKupac.pdf");
								p.waitFor();
					}else if(parametar.contains("IOSDobavljac")){
						Process p = Runtime
								   .getRuntime()
								   .exec("rundll32 url.dll,FileProtocolHandler E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\Reports\\IOSDobavljac.pdf");
								p.waitFor();
					}
				} else {

					System.out.println("File is not exists");

				}

				System.out.println("Done");

		  	  } catch (Exception ex) {
				ex.printStackTrace();
			  }
		
	/*
		String filename="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.pdf";
		File file = new File(filename);
		 if(file != null) {
		        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		        if (mimeType == null) {
		  //          logger.debug("mimetype is not detectable, will take default");
		            mimeType = "application/pdf";
		        }
		  //      logger.debug("mimetype : {}", mimeType);
		        response.setContentType(mimeType);
		        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
		        response.setContentLength((int) file.length());
		        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		        FileCopyUtils.copy(inputStream, response.getOutputStream());
		}
		 */
		/*try {
			String filename="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.pdf";
			Path pdfPath = Paths.get(filename);
		    System.out.println(pdfPath+"PDF PATH ");
		    byte[] documentInBytes = Files.readAllBytes(pdfPath);
	     //  byte[] documentInBytes = getDocument();         
	//        response.setHeader("Content-Disposition", "inline; filename=\"report.pdf\"");
	        response.setDateHeader("Expires", -1);
	        response.setContentType("application/pdf");
	        response.setContentLength(documentInBytes.length);
	        response.getOutputStream().write(documentInBytes);
	    } catch (Exception ioe) {
	    } finally {
	    }
	    return null;
		*/
		/*	
			InputStream in=new FileInputStream(htmlpath);
	        int len = -1;
	        byte[]  byt = new byte[1024];

	        ServletOutputStream outputStream = response.getOutputStream();
	        while((len = in.read(byt)) != -1){
	            outputStream.write(byt, 0, len);
	            System.out.println("WRITING");
	        }
	        outputStream.flush();
	        outputStream.close();
	        in.close();
			*/
			/*
			
			
			        String mimeType = URLConnection.guessContentTypeFromName(filename);
			        if (mimeType == null) {
			            mimeType = "application/pdf";
			        }
			        response.setContentType(mimeType);
			        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", filename));
			        InputStream inputStream = new BufferedInputStream(new FileInputStream(filename));
			        FileCopyUtils.copy(inputStream, response.getOutputStream());
			        
			        */
		/*	
		String filename="E:\\Faks\\4. god\\ProjekatPoslovna\\repo3\\src\\main\\webapp\\WEB-INF\\views\\test_jasper.pdf";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.parseMediaType("application/pdf"));
		    String fajl = "test_jasper.pdf";
		 //   headers.setContentDispositionFormData(filename, filename);
		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		  //  headers.setContentDispositionFormData(fajl, fajl);
		    headers.add("content-disposition", "attachment; filename=" + fajl);
		//    headers.setContentDispositionFormData("inline", fajl);
		//    response.setHeader("Content-Disposition",  "inline");
		 //   headers.add("content-disposition", "inline;filename=" + "test_jasper.pdf");
		 //   headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		   
		    Path pdfPath = Paths.get(filename);
		    System.out.println(pdfPath+"PDF PATH ");
		    byte[] pdf = Files.readAllBytes(pdfPath);
		    System.out.println(pdf.length+"DUZINA BAJTOVA ");
		    ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
		    System.out.println(response.getHeaders()+"hederi wateva");
		    System.out.println(response.getBody());
		    return response; */
	
	}
}
