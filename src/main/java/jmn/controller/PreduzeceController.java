package jmn.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.configuration.AES;
import jmn.models.Adresa;
import jmn.models.DEK;
import jmn.models.Preduzece;
import jmn.models.SifrarnikDelatnosti;
import jmn.pojo.UserData;
import jmn.services.AdresaService;
import jmn.services.DEKService;
import jmn.services.PreduzeceService;
import jmn.services.SifrarnikDelatnostiService;

@Controller
public class PreduzeceController {
	
	@Autowired
	private PreduzeceService ps;
	
	@Autowired
	private SifrarnikDelatnostiService sds;
	
	@Autowired
	private AdresaService as;
	
	@Autowired
	private DEKService dks;
	
	@PermissionType("Preduzeca:view")
	@RequestMapping(value="/preduzeca/", method=RequestMethod.GET)
	public ResponseEntity<HashMap<String, Object>> fetchPreduzeca(HttpSession session){
		List<Adresa> adrese = as.findAll();
		List<Preduzece> preduzeca = ps.findAll();
		List<SifrarnikDelatnosti> delatnosti = sds.findAll();
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("adrese", adrese);
		resp.put("delatnosti", delatnosti);
		if(preduzeca.isEmpty())
			return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
		resp.put("preduzeca", preduzeca);
		return new ResponseEntity<HashMap<String, Object>>(resp, HttpStatus.OK);
	}
	
	@PermissionType("Preduzeca:add")
	@RequestMapping(value = "/preduzeca/", method = RequestMethod.POST)
	public ResponseEntity<?> createPreduzece(HttpSession session, @RequestBody Preduzece preduzece, UriComponentsBuilder ucBuilder) {
			Preduzece p = ps.findByPib(preduzece.getPib());
			String error = "";
			if(p != null){
				error = "pib mora biti jedinstven";
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.TEXT_PLAIN);
				return new ResponseEntity<String>(error, headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			p = ps.findByMaticniBroj(preduzece.getMaticniBroj());
			
			if(p != null){
				error = "maticni broj mora biti jedinstven";
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.TEXT_PLAIN);
				return new ResponseEntity<String>(error, headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			 String dk = AES.initDEK();
			 int pb = p.getPib();
			 String rez = AES.encryptDEK(dk, Integer.toString(pb));
			 String iv = AES.encodeIV();
			 DEK d = new DEK(rez, iv, p);
			 d = dks.saveDEK(d);
			 p = ps.savePreduzece(preduzece); 
	        
			try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/preduzeca/{id}").buildAndExpand(p.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("Preduzeca:edit")
	 @RequestMapping(value = "/preduzeca/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updatePreduzece(HttpSession session, @PathVariable("id") long id, @RequestBody Preduzece preduzece) {
			UserData u = (UserData)session.getAttribute("user");
			
			Preduzece exists = ps.findOne(id);
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	Adresa a = as.findOne(exists.getAdresa().getId());
		 	SifrarnikDelatnosti d = sds.findOne(exists.getSifraDelatnosti().getId());
		 	DEK dk = dks.findByPreduzece(exists);
		 	String val = AES.decryptDEK(dk.getValue(), Integer.toString(exists.getPib()));
		 	exists.setAdresa(a);;
		 	exists.setSifraDelatnosti(d);
		 	exists.setBrTelefona(preduzece.getBrTelefona());
		 	exists.setEmail(preduzece.getEmail());
		 	exists.setMaticniBroj(preduzece.getMaticniBroj());
		 	exists.setFax(preduzece.getFax());
		 	exists.setNaziv(preduzece.getNaziv());
		 	exists.setPib(preduzece.getPib());
		 	exists = ps.savePreduzece(exists);
		 	dk.setValue(AES.encryptDEK(val, Integer.toString(exists.getPib())));
		 	dks.saveDEK(dk);
		 	return new ResponseEntity<Preduzece>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("Preduzeca:delete")
	@RequestMapping(value = "/preduzeca/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deletePreduzece(HttpSession session, @PathVariable("id") long id) {
		Preduzece exists = ps.findOne(id); 
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 ps.delete(id);
	     return new ResponseEntity<Preduzece>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("Preduzeca:search")
	@RequestMapping(value = "/preduzeca/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchPreduzeca(HttpSession session, @RequestBody Preduzece partner) {
			List<Preduzece> preduzeca = null;
			if(partner.getNaziv() == null)
				partner.setNaziv("");
			if(partner.getPib() == -1)
				preduzeca = ps.findByNazivAndPib(partner.getNaziv(),partner.getPib());
				
		 if(preduzeca == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<Preduzece>>(preduzeca, HttpStatus.OK);
	    }
}
