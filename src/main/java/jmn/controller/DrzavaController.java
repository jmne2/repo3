package jmn.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import jmn.annotation.PermissionType;
import jmn.models.Drzava;
import jmn.services.DrzavaService;

@Controller

public class DrzavaController {

	@Autowired
	private DrzavaService drz;

	@PermissionType("Drzave:view")
	@RequestMapping(value="/drzave/", method=RequestMethod.GET)
	public ResponseEntity<List<Drzava>> fetchDrzave(HttpSession session){
		List<Drzava> drzave = drz.findAll();
		if(drzave.isEmpty())
			return new ResponseEntity<List<Drzava>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Drzava>>(drzave, HttpStatus.OK);
	}
	
	@PermissionType("Drzave:add")
	@RequestMapping(value = "/drzave/", method = RequestMethod.POST)
	public ResponseEntity<Void> createDrzava(HttpSession session, @RequestBody Drzava drzava, UriComponentsBuilder ucBuilder) {
	        System.out.println("Creating Drzava " + drzava.getOznaka());
	  
	        Drzava d = drz.saveDrzava(drzava);
	        try {
		        HttpHeaders headers = new HttpHeaders();
		        headers.setLocation(ucBuilder.path("/drzave/{id}").buildAndExpand(d.getId()).toUri());
		        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	        }
	        catch(Exception e){
	        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        }
	    }
	@PermissionType("Drzave:edit")
	 @RequestMapping(value = "/drzave/{id}", method = RequestMethod.POST)
	    public ResponseEntity<?> updateDrzava(HttpSession session, @PathVariable("id") long id, @RequestBody Drzava drzava) {
	 
		 	Drzava exists = drz.findOne(id);
		 	if(exists == null){
		 		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 	}
		 	exists.setOznaka(drzava.getOznaka());
		 	exists.setNaziv(drzava.getNaziv());
		 	exists = drz.saveDrzava(exists);
		 	return new ResponseEntity<Drzava>(exists, HttpStatus.OK);
	    }
	
	@PermissionType("Drzave:delete")
	@RequestMapping(value = "/drzave/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> deleteDrzava(HttpSession session, @PathVariable("id") long id) {
		 Drzava exists = drz.findOne(id);
		 if(exists == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		 
		 drz.delete(id);
	     return new ResponseEntity<Drzava>(HttpStatus.NO_CONTENT);
	    }
	
	@PermissionType("Drzave:search")
	@RequestMapping(value = "/drzave/search/", method = RequestMethod.POST)
	    public ResponseEntity<?> searchDrzave(HttpSession session, @RequestBody Drzava drzava) {
		 if(drzava.getOznaka() == null)
			 drzava.setOznaka("");
		 if(drzava.getNaziv() == null)
			 drzava.setNaziv("");
		 List<Drzava> drzave = drz.findByOznakaAndNaziv(drzava.getOznaka(), drzava.getNaziv());
		 if(drzave == null)
			 return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);

	     return new ResponseEntity<List<Drzava>>(drzave, HttpStatus.OK);
	    }
}
