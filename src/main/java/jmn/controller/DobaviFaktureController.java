package jmn.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jmn.annotation.PermissionType;
import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.RacunPartnera;
import jmn.models.StavkaFakture;
import jmn.pojo.UserData;
import jmn.rest.FakturaRS;
import jmn.rest.FaktureRS;
import jmn.rest.StavkaFaktureRS;
import jmn.services.FakturaService;
import jmn.services.PoslovnaGodinaService;
import jmn.services.PoslovniPartnerService;
import jmn.services.PreduzeceService;
import jmn.services.RacunPartneraService;
import jmn.services.StavkaFaktureService;

@Controller
public class DobaviFaktureController {
	
	@Autowired
	private PoslovniPartnerService pps;
	
	@Autowired
	private PreduzeceService ps;

	@Autowired
	private RacunPartneraService rps;
	
	@Autowired
	private PoslovnaGodinaService psr;
	
	@Autowired
	private FakturaService fs;
	
	@Autowired
	private StavkaFaktureService sfs;
	
	public static String BASE_URL = "http://localhost:8080/";
	
	@PermissionType("Fakture:view")
	@RequestMapping(value="/askFakture/{id}", method=RequestMethod.POST)
	public ResponseEntity<?> fetchNoveFakture(HttpSession session, @PathVariable("id") Long id){
			UserData u = (UserData)session.getAttribute("user");
			Preduzece p = u.getPreduzece();
			PoslovniPartner pp = pps.findOne(id);
			Client client = ClientBuilder.newClient();
			WebTarget target = client.target("http://localhost:8080/sendFakture/" + pp.getPib() + "/" + p.getPib());
			FaktureRS s = target.request().get(FaktureRS.class);
			//URL url = new URL(BASE_URL + "sendFakture/1");
			//InputStream in = url.openStream();
	        //System.out.println(new String(IOUtils.toByteArray(in), "UTF-8")); //
			System.out.println(s.getFakture().size());
			for(FakturaRS f : s.getFakture()){
				Faktura faktura = new Faktura();
				Preduzece preduzece = ps.findByPib(Integer.parseInt(f.getPibKupca()));
				PoslovniPartner parnter = pps.findByPib(Integer.parseInt(f.getPibDobavljaca()));
				faktura.setPreduzece(preduzece);
				faktura.setPoslovniPartner(parnter);
				faktura.setBrojFakture(f.getBrRacuna());
				
				java.util.Date utilDate = f.getDatumRacuna();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				final String stringDate= dateFormat.format(utilDate);
				final java.sql.Date sqlDate=  java.sql.Date.valueOf(stringDate);
		
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(utilDate);
				int year = calendar.get(Calendar.YEAR);
				SimpleDateFormat df = new SimpleDateFormat("yyyy");
				
				faktura.setDatumFakture(sqlDate);
				
				java.util.Date utilDate1 = f.getDatumValute();
				SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
				final String stringDate1= dateFormat1.format(utilDate1);
				final java.sql.Date sqlDate1=  java.sql.Date.valueOf(stringDate1);
				
				faktura.setDatumValute(sqlDate1);
				
				faktura.setOznakaValute(f.getOznakaValute());
				faktura.setVrstaFakture("ulazne");
				faktura.setVrednostUsluga(f.getVrednostUsluga());
				faktura.setVrednostRobe(f.getVrednostRobe());
				faktura.setUkupnoRobaIUsluge(f.getUkupnoRobaIUsluge());
				faktura.setUkupanRabat(f.getUkupanRabat());
				faktura.setUkupanPorez(f.getUkupanPorez());
				faktura.setUkIznos(f.getUkIznos());
				RacunPartnera racunPartnera = rps.findByBrRacuna(f.getUplataNaRacun());
				faktura.setRacunPartnera(racunPartnera);
				faktura.setPreostaliIznos(f.getUkIznos());
				PoslovnaGodina polovnaGodina = psr.findByGodinaAndPreduzece(year, preduzece);
				faktura.setPoslovnaGodina(polovnaGodina);
				
				fs.saveFaktura(faktura);
				
				for (StavkaFaktureRS stF : f.getProducts()) {
					StavkaFakture stavkaFakt = new StavkaFakture();
					stavkaFakt.setIznosRabata(stF.getIznostRabata());
					stavkaFakt.setJedinicaMere(stF.getJedinicaMere());
					stavkaFakt.setJedinicnaCena(stF.getJedinicnaCena());
					stavkaFakt.setKolicina(stF.getKolicina());
					stavkaFakt.setNazivRobeUsluge(stF.getNazivRobe());
					stavkaFakt.setProcenatRabata(stF.getProcenaRabata());
					stavkaFakt.setRedniBroj(stF.getRedniBroj());
					stavkaFakt.setStavkaFakture(faktura);
					stavkaFakt.setUkupanPorez(stF.getUkupanPorez());
					stavkaFakt.setUmanjenoZaRabat(stF.getUmanjenoZaRabat());
					stavkaFakt.setVrednost(stF.getVrednost());
					sfs.saveStavkaFakture(stavkaFakt);
				}
			}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
