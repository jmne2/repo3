package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.DnevnoStanje;
import jmn.models.PoslovniPartner;
import jmn.models.StavkaIzvoda;
import jmn.repositories.StavkaIzvodaRepository;

@Service
public class StavkaIzvodaServiceImpl implements StavkaIzvodaService{

	@Autowired
	private StavkaIzvodaRepository sir;
	
	@Override
	public List<StavkaIzvoda> findAll() {
		// TODO Auto-generated method stub
		return (List<StavkaIzvoda>)sir.findAll();
	}

	@Override
	public StavkaIzvoda saveStavkaIzvoda(StavkaIzvoda si) {
		// TODO Auto-generated method stub
		return sir.save(si);
	}

	@Override
	public StavkaIzvoda findOne(Long id) {
		// TODO Auto-generated method stub
		return sir.findOne(id);
	}

	@Override
	public void delete(Long id) {
		sir.delete(id);
	}

	@Override
	public void delete(StavkaIzvoda si) {
		sir.delete(si);
	}

	@Override
	public List<StavkaIzvoda> findByDnevnoStanje(DnevnoStanje dnevnoStanje) {
		// TODO Auto-generated method stub
		return sir.findByDnevnoStanje(dnevnoStanje);
	}

	@Override
	public List<StavkaIzvoda> findByDnevnoStanjeAndSmerIgnoreCase(DnevnoStanje dnevnoStanje, String smer) {
		return sir.findByDnevnoStanjeAndSmerIgnoreCase(dnevnoStanje, smer);
	}

	
}
