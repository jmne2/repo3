package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Drzava;
import jmn.models.NaseljenoMesto;
import jmn.repositories.NaseljenoMestoRepository;

@Service
public class NaseljenoMestoServiceImpl implements NaseljenoMestoService{

	@Autowired
	private NaseljenoMestoRepository nasRepo;
	
	@Override
	public List<NaseljenoMesto> findAll() {
		return (List<NaseljenoMesto>) nasRepo.findAll();
	}

	@Override
	public NaseljenoMesto save(NaseljenoMesto nas) {
		return nasRepo.save(nas);
	}

	@Override
	public NaseljenoMesto findOne(Long id) {
		return nasRepo.findOne(id);
	}

	@Override
	public void delete(Long id) {
		nasRepo.delete(id);
	}
	
	@Override
	public void delete(NaseljenoMesto nas) {
		nasRepo.delete(nas);
	}

	@Override
	public List<NaseljenoMesto> findByNazivContainingAndPostBrojContainingAndDrzava(String naziv, String postBroj, Drzava drzava) {
		return nasRepo.findByNazivContainingAndPostBrojContainingAndDrzava(naziv, postBroj, drzava);
	}

	@Override
	public List<NaseljenoMesto> findByNazivContainingAndPostBrojContaining(String naziv, String postBroj) {
		return nasRepo.findByNazivContainingAndPostBrojContaining(naziv, postBroj);
	}


}
