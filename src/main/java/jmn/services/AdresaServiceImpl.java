package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Adresa;
import jmn.models.NaseljenoMesto;
import jmn.repositories.AdresaRepository;

@Service
public class AdresaServiceImpl implements AdresaService{

	@Autowired
	private AdresaRepository ar;
	
	@Override
	public List<Adresa> findAll() {
		return (List<Adresa>)ar.findAll();
	}

	@Override
	public Adresa saveAdresa(Adresa adr) {
		return ar.save(adr);
	}

	@Override
	public Adresa findOne(Long id) {
		return ar.findOne(id);
	}

	@Override
	public void delete(Long id) {
		ar.delete(id);
	}

	@Override
	public void delete(Adresa adr) {
		ar.delete(adr);
	}

	@Override
	public List<Adresa> findByUlicaContainingAndBrojAndNasMesto(String ulica, int broj, NaseljenoMesto nasMesto) {
		return ar.findByUlicaContainingAndBrojAndNasMesto(ulica, broj, nasMesto);
	}

	@Override
	public List<Adresa> findByUlicaContainingAndNasMesto(String ulica, NaseljenoMesto nasMesto) {
		return ar.findByUlicaContainingAndNasMesto(ulica, nasMesto);
	}

	@Override
	public List<Adresa> findByUlicaContainingAndBroj(String ulica, int broj) {
		return ar.findByUlicaContainingAndBroj(ulica, broj);
	}

	@Override
	public List<Adresa> findByUlicaContaining(String ulica) {
		return ar.findByUlicaContaining(ulica);
	}

}
