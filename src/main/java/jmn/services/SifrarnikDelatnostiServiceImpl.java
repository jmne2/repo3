package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.SifrarnikDelatnosti;
import jmn.repositories.SifrarnikDelatnostiRepository;

@Service
public class SifrarnikDelatnostiServiceImpl implements SifrarnikDelatnostiService{

	@Autowired
	private SifrarnikDelatnostiRepository sdr;
	
	@Override
	public List<SifrarnikDelatnosti> findAll() {
		// TODO Auto-generated method stub
		return (List<SifrarnikDelatnosti>)sdr.findAll();
	}

	@Override
	public SifrarnikDelatnosti saveSifrarnikDelatnosti(SifrarnikDelatnosti sd) {
		// TODO Auto-generated method stub
		return sdr.save(sd);
	}

	@Override
	public SifrarnikDelatnosti findOne(Long id) {
		// TODO Auto-generated method stub
		return sdr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		sdr.delete(id);
	}

	@Override
	public void delete(SifrarnikDelatnosti sd) {
		// TODO Auto-generated method stub
		sdr.delete(sd);
	}

	@Override
	public List<SifrarnikDelatnosti> findByNazivDelatnostiAndSifra(String nazivDelatnosti, String sifra) {
		return sdr.findByNazivDelatnostiContainingAndSifraContaining(nazivDelatnosti, sifra);
	}

	
}
