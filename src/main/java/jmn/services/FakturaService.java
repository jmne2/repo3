package jmn.services;

import java.sql.Date;
import java.util.List;

import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.RacunPartnera;

public interface FakturaService {

	List<Faktura> findAll();

	Faktura saveFaktura(Faktura izf);

	Faktura findOne(Long id);

	void delete(Long id);

	void delete(Faktura izf);

	List<Faktura> findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(Long brojFakture, PoslovnaGodina poslovnaGodina,
			PoslovniPartner poslovniPartner);

	List<Faktura> findByBrojFaktureAndPoslovnaGodina(Long brojFakture, PoslovnaGodina poslovnaGodina);

	List<Faktura> findByBrojFaktureAndPoslovniPartner(Long brojFakture, PoslovniPartner poslovniPartner);

	List<Faktura> findByVrstaFaktureAndPreduzece(String vrstaFakture, Preduzece preduzece);

	List<Faktura> findByPreduzeceAndBrojFaktureAndVrstaFakture(Preduzece preduzece, Long brojFakture,
			String vrstaFakture);

	List<Faktura> findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(Preduzece preduzece, String vrstaFakture,
			double preostaliIznos);

	List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(Preduzece preduzece,
			String vrstaFakture, PoslovniPartner poslovniPartner, double preostaliIznos);

	List<Faktura> findByBrojFaktureAndPreduzeceAndPoslovniPartnerAndRacunPartneraAndVrstaFakture(Long brojFakture,
			Preduzece preduzece, PoslovniPartner poslovniPartner, RacunPartnera racunPartnera, String vrstaFakture);

	List<Faktura> findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFakture(PoslovniPartner poslovniPartner,
			Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture);

	List<Faktura> findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFaktureAndDatumFakture(
			PoslovniPartner poslovniPartner, Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture,
			Date datumFakture);

	List<Faktura> findByPreduzeceAndDatumFaktureAndVrstaFakture(Preduzece preduzece, Date datumFakture, String vrstaFakture);
	
	Faktura findByBrojFaktureAndPreduzeceAndDatumFakture(Long brojFakture, Preduzece preduzece, Date datumFakture);
	
	List<Faktura> findByPreduzeceAndRacunPartneraAndVrstaFakture(Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture);
	List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndBrojFakture(Preduzece preduzece, String vrstaFakture,PoslovniPartner poslovniPartner, Long brojFakture);

}
