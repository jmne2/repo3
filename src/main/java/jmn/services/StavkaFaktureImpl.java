package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Faktura;
import jmn.models.StavkaFakture;
import jmn.repositories.StavkaFaktureRepository;

@Service
public class StavkaFaktureImpl implements StavkaFaktureService{

	@Autowired 
	private StavkaFaktureRepository sfs;
	
	@Override
	public List<StavkaFakture> findAll() {
		return (List<StavkaFakture>) sfs.findAll();
	}

	@Override
	public StavkaFakture saveStavkaFakture(StavkaFakture stFa) {
		return sfs.save(stFa);
	}

	@Override
	public StavkaFakture findOne(Long id) {
		return sfs.findOne(id);
	}

	@Override
	public void delete(Long id) {
		sfs.delete(id);
	}

	@Override
	public void delete(StavkaFakture stavkaFakture) {
		sfs.delete(stavkaFakture);
	}

	@Override
	public List<StavkaFakture> findByStavkaFakture(Faktura stavkaFakture) {
		return sfs.findByStavkaFakture(stavkaFakture);
	}

	@Override
	public List<StavkaFakture> findByJedinicaMereAndJedinicnaCenaAndKolicinaAndNazivRobeUslugeContainingAndProcenatRabataAndUkupanPorezAndVrednostAndStavkaFakture(
			double jedinicaMere, double jedinicnaCena, double kolicina, String nazivRobeIUsluga, double procenatRabata,
			double ukupanPorez, double vrednost, Faktura stavkaFakture) {
		return sfs.findByJedinicaMereAndJedinicnaCenaAndKolicinaAndNazivRobeUslugeContainingAndProcenatRabataAndUkupanPorezAndVrednostAndStavkaFakture(jedinicaMere, jedinicnaCena, kolicina, nazivRobeIUsluga, procenatRabata, ukupanPorez, vrednost, stavkaFakture);
	}

}
