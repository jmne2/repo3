package jmn.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.RacunPartnera;
import jmn.repositories.FakturaRepository;

@Service
public class FakturaServiceImpl implements FakturaService{

	@Autowired
	private FakturaRepository izfr;
	
	@Override
	public List<Faktura> findAll() {
		return (List<Faktura>) izfr.findAll();
	}

	@Override
	public Faktura saveFaktura(Faktura izf) {
		return izfr.save(izf);
	}

	@Override
	public Faktura findOne(Long id) {
		return izfr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		izfr.delete(id);
	}

	@Override
	public void delete(Faktura izf) {
		izfr.delete(izf);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(Long brojFakture,
			PoslovnaGodina poslovnaGodina, PoslovniPartner poslovniPartner) {
		return izfr.findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(brojFakture, poslovnaGodina, poslovniPartner);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPoslovnaGodina(Long brojFakture, PoslovnaGodina poslovnaGodina) {
		return izfr.findByBrojFaktureAndPoslovnaGodina(brojFakture, poslovnaGodina);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPoslovniPartner(Long brojFakture, PoslovniPartner poslovniPartner) {
		return izfr.findByBrojFaktureAndPoslovniPartner(brojFakture, poslovniPartner);
	}

	@Override
	public List<Faktura> findByVrstaFaktureAndPreduzece(String vrstaFakture, Preduzece preduzece) {
		return izfr.findByVrstaFaktureAndPreduzece(vrstaFakture, preduzece);
	}

	@Override
	public List<Faktura> findByPreduzeceAndBrojFaktureAndVrstaFakture(Preduzece preduzece, Long brojFakture,
			String vrstaFakture) {
		return izfr.findByPreduzeceAndBrojFaktureAndVrstaFakture(preduzece, brojFakture, vrstaFakture);
	}

	@Override
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture, double preostaliIznos) {
		return izfr.findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(preduzece,vrstaFakture, preostaliIznos);
	}

	@Override
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(
			Preduzece preduzece, String vrstaFakture, PoslovniPartner poslovniPartner, double preostaliIznos) {
		return izfr.findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(preduzece, vrstaFakture, poslovniPartner, preostaliIznos);
	}

	@Override
	public List<Faktura> findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFakture(
			PoslovniPartner poslovniPartner, Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture) {
		return izfr.findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFakture(poslovniPartner, preduzece, racunPartnera, vrstaFakture);
	}

	@Override
	public List<Faktura> findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFaktureAndDatumFakture(
			PoslovniPartner poslovniPartner, Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture,
			Date datumFakture) {
		// TODO Auto-generated method stub
		return izfr.findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFaktureAndDatumFakture(poslovniPartner, preduzece, racunPartnera, vrstaFakture, datumFakture);
	}

	@Override
	public List<Faktura> findByPreduzeceAndDatumFaktureAndVrstaFakture(Preduzece preduzece, Date datumFakture, String vrstaFakture){
		// TODO Auto-generated method stub
		return izfr.findByPreduzeceAndDatumFaktureAndVrstaFakture(preduzece,datumFakture,vrstaFakture);
	}

	@Override
	public List<Faktura> findByBrojFaktureAndPreduzeceAndPoslovniPartnerAndRacunPartneraAndVrstaFakture(
			Long brojFakture, Preduzece preduzece, PoslovniPartner poslovniPartner, RacunPartnera racunPartnera,
			String vrstaFakture) {
		// TODO Auto-generated method stub
		return izfr.findByBrojFaktureAndPreduzeceAndPoslovniPartnerAndRacunPartneraAndVrstaFakture(brojFakture, preduzece, poslovniPartner, racunPartnera, vrstaFakture);
	}

	@Override
	public Faktura findByBrojFaktureAndPreduzeceAndDatumFakture(Long brojFakture, Preduzece preduzece, Date datumFakture) {
		// TODO Auto-generated method stub
		return izfr.findByBrojFaktureAndPreduzeceAndDatumFakture( brojFakture, preduzece, datumFakture);
	}

	@Override
	public List<Faktura> findByPreduzeceAndRacunPartneraAndVrstaFakture(Preduzece preduzece,
			RacunPartnera racunPartnera, String vrstaFakture) {
		// TODO Auto-generated method stub
		return izfr.findByPreduzeceAndRacunPartneraAndVrstaFakture( preduzece, racunPartnera, vrstaFakture);
	}
	@Override
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndBrojFakture(Preduzece preduzece,
			String vrstaFakture, PoslovniPartner poslovniPartner, Long brojFakture) {
		return izfr.findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndBrojFakture(preduzece, vrstaFakture, poslovniPartner, brojFakture);
	}

}
