package jmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.repositories.PoslovniPartnerRepository;

@Service
public class PoslovniPartnerServiceImpl implements PoslovniPartnerService{

	@Autowired
	private PoslovniPartnerRepository ppr;
	
	@Override
	public List<PoslovniPartner> findAll() {
		return (List<PoslovniPartner>) ppr.findAll();
	}

	@Override
	public PoslovniPartner savePoslovniPartner(PoslovniPartner pp) {
		return ppr.save(pp);
	}

	@Override
	public PoslovniPartner findOne(Long id) {
		return ppr.findOne(id);
	}

	@Override
	public void delete(Long id) {
		ppr.delete(id);
	}

	@Override
	public void delete(PoslovniPartner pp) {
		ppr.delete(pp);
	}

	@Override
	public List<PoslovniPartner> findByNazivAndPibAndPreduzece(String naziv, int pib, Preduzece preduzece) {
		return ppr.findByNazivAndPibAndPreduzece(naziv, pib, preduzece);
	}

	@Override
	public List<PoslovniPartner> findByNazivAndPreduzece(String naziv, Preduzece preduzece) {
		return ppr.findByNazivAndPreduzece(naziv, preduzece);
	}

	@Override
	public PoslovniPartner findByPib(int pib) {
		return ppr.findByPib(pib);
	}

	@Override
	public PoslovniPartner findByMaticniBroj(Long maticniBroj) {
		return ppr.findByMaticniBroj(maticniBroj);
	}

	@Override
	public List<PoslovniPartner> findByPreduzece(Preduzece preduzece) {
		return ppr.findByPreduzece(preduzece);
	}

	@Override
	public List<PoslovniPartner> findByPreduzeceAndVrsta(Preduzece preduzece, String vrsta) {
		return ppr.findByPreduzeceAndVrsta(preduzece, vrsta);
	}

}
