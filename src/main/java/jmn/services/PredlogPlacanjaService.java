package jmn.services;

import java.util.Date;
import java.util.List;

import jmn.models.PredlogPlacanja;
import jmn.models.Preduzece;

public interface PredlogPlacanjaService {

	List<PredlogPlacanja> findAll();

	PredlogPlacanja savePredlogPlacanja(PredlogPlacanja p);

	PredlogPlacanja findOne(Long id);

	void delete(Long id);

	void delete(PredlogPlacanja p);

	List<PredlogPlacanja> findByDatumAndStatusContainingAndBrojContainingAndPreduzece(Date datum, String status,
			String broj, Preduzece preduzece);

	List<PredlogPlacanja> findByStatusContainingAndBrojContainingAndPreduzece(String status, String broj, Preduzece preduzece);
	
	List<PredlogPlacanja> findByPreduzece(Preduzece preduzece);
	
	List<PredlogPlacanja> findByStatusAndPreduzece(String status, Preduzece preduzece);
}
