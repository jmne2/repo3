package jmn.services;

import java.util.List;

import jmn.models.SifrarnikDelatnosti;

public interface SifrarnikDelatnostiService {

	List<SifrarnikDelatnosti> findAll();

	SifrarnikDelatnosti saveSifrarnikDelatnosti(SifrarnikDelatnosti sd);

	SifrarnikDelatnosti findOne(Long id);

	void delete(Long id);

	void delete(SifrarnikDelatnosti sd);
	
	List<SifrarnikDelatnosti> findByNazivDelatnostiAndSifra(String nazivDelatnosti, String sifra);
}
