package jmn.services;

import java.util.List;

import jmn.models.Role;

public interface RoleService {

	List<Role> findAll();

	Role saveRole(Role role);

	Role findOne(Long id);

	void delete(Long id);

	void delete(Role role);
	
	Role findByNaziv(String naziv);
}
