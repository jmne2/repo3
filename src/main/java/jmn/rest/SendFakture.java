package jmn.rest;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Pattern;
import javax.ws.rs.Produces;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendFakture {

	private Map<Long, FaktureRS> customers = new HashMap<Long, FaktureRS>();
	
	
	public SendFakture() {
		init();
	}


    @Produces("application/xml")
	@RequestMapping(value="/sendFakture/{id}/{pib}", method=RequestMethod.GET)
    public FaktureRS getCustomer(@PathVariable("id") @Pattern(regexp = "[0-9]+", message = "The id must be a valid number") String id, @PathVariable("pib") String pib) {
        System.out.println("Got this " + id + " " + pib);
        Long idNumber = Long.valueOf(id);
        FaktureRS temp = customers.get(idNumber);
        FaktureRS c = new FaktureRS();
        for(FakturaRS f : temp.getFakture()){
        	if(f.getPibKupca().equals(pib))
        		c.getFakture().add(f);
        }
        return c;
    }
    
    private void init(){
    	FakturaRS fakt = new FakturaRS();
		fakt.setId(new Long(1));
		fakt.setAdresaDobavljaca("Chantry Pass 1");
		fakt.setAdresaKupca("Redflow 3");
		fakt.setBrRacuna(new Long(12));
		fakt.setDatumRacuna(new java.util.Date());
		fakt.setDatumValute(new java.util.Date());
		fakt.setNazivDobavljaca("Emreis Navigation");
		fakt.setNazivKupca("Foltest the Last");
		fakt.setOznakaValute("RSD");
		fakt.setPibDobavljaca("332147885");
		fakt.setPibKupca("183466789");
		fakt.setPreostaliIznos(5000);
		fakt.setUkIznos(8900);
		fakt.setUkupanPorez(430);
		fakt.setUkupanRabat(300);
		fakt.setUkupnoRobaIUsluge(9000);
		fakt.setUplataNaRacun("456456789222222222");
		fakt.setVrednostRobe(4360);
		fakt.setVrednostUsluga(1478);
		StavkaFaktureRS stavka = new StavkaFaktureRS();
		stavka.setIznostRabata(3200);
		stavka.setJedinicaMere("komadi");
		stavka.setJedinicnaCena(200);
		stavka.setKolicina(14);
		stavka.setNazivRobe("Chests");
		stavka.setProcenaRabata(12);
		stavka.setRedniBroj(new Long(1));
		stavka.setUkupanPorez(150);
		stavka.setUmanjenoZaRabat(0);
		stavka.setVrednost(3600);
		fakt.getProducts().add(stavka);
		
		FakturaRS faktura1 = new FakturaRS();
		faktura1.setId(new Long(2));
		faktura1.setAdresaDobavljaca("Chantry Pass 1");
		faktura1.setAdresaKupca("Redflow 3");
		faktura1.setBrRacuna(new Long(12));
		faktura1.setDatumRacuna(new java.util.Date());
		faktura1.setDatumValute(new java.util.Date());
		faktura1.setNazivDobavljaca("Emreis Navigation");
		faktura1.setNazivKupca("Foltest the Last");
		faktura1.setOznakaValute("RSD");
		faktura1.setPibDobavljaca("332147885");
		faktura1.setPibKupca("183466789");
		faktura1.setPreostaliIznos(5000);
		faktura1.setUkIznos(8900);
		faktura1.setUkupanPorez(430);
		faktura1.setUkupanRabat(300);
		faktura1.setUkupnoRobaIUsluge(9000);
		faktura1.setUplataNaRacun("456456789222222222");
		faktura1.setVrednostRobe(4360);
		faktura1.setVrednostUsluga(1478);
		StavkaFaktureRS stavka2 = new StavkaFaktureRS();
		stavka2.setIznostRabata(1200);
		stavka2.setJedinicaMere("kg");
		stavka2.setJedinicnaCena(600);
		stavka2.setKolicina(30);
		stavka2.setNazivRobe("Nilfgardian Meat");
		stavka2.setProcenaRabata(40);
		stavka2.setRedniBroj(new Long(1));
		stavka2.setUkupanPorez(400);
		stavka2.setUmanjenoZaRabat(0);
		stavka2.setVrednost(8750);
		faktura1.getProducts().add(stavka2);
		StavkaFaktureRS stavka3 = new StavkaFaktureRS();
		stavka3.setIznostRabata(2130);
		stavka3.setJedinicaMere("m");
		stavka3.setJedinicnaCena(720);
		stavka3.setKolicina(90);
		stavka3.setNazivRobe("Kovir Silk");
		stavka3.setProcenaRabata(60);
		stavka3.setRedniBroj(new Long(2));
		stavka3.setUkupanPorez(632);
		stavka3.setUmanjenoZaRabat(2);
		stavka3.setVrednost(12780);
		faktura1.getProducts().add(stavka3);
		
		//////////
		FaktureRS fakture = new FaktureRS();
		fakture.getFakture().add(fakt);
		fakture.getFakture().add(faktura1);
		customers.put(new Long(332147885), fakture);
		
		FakturaRS faktura2 = new FakturaRS();
		faktura2.setId(new Long(5));
		faktura2.setAdresaKupca("Radiant Blvd 24");
		faktura2.setBrRacuna(new Long(25));
		faktura2.setDatumRacuna(new java.util.Date());
		faktura2.setDatumValute(new java.util.Date());
		faktura2.setNazivKupca("Foltest the Last");
		faktura2.setOznakaValute("RSD");
		faktura2.setPibDobavljaca("428847801");
		faktura2.setPibKupca("183466990");
		faktura2.setPreostaliIznos(5000);
		faktura2.setUkIznos(8900);
		faktura2.setUkupanPorez(430);
		faktura2.setUkupanRabat(300);
		faktura2.setUkupnoRobaIUsluge(9000);
		faktura2.setVrednostRobe(10360);
		faktura2.setVrednostUsluga(1478);
		faktura2.setAdresaDobavljaca("Swartabog 1");
		faktura2.setNazivDobavljaca("Lion Transportation Co.");
		faktura2.setUplataNaRacun("234905481174550070");
		StavkaFaktureRS stavka4 = new StavkaFaktureRS();
		stavka4.setIznostRabata(2130);
		stavka4.setJedinicaMere("m");
		stavka4.setJedinicnaCena(720);
		stavka4.setKolicina(120);
		stavka4.setNazivRobe("Kovir Silk");
		stavka4.setProcenaRabata(60);
		stavka4.setRedniBroj(new Long(1));
		stavka4.setUkupanPorez(700);
		stavka4.setUmanjenoZaRabat(4);
		stavka4.setVrednost(15780);
		faktura2.getProducts().add(stavka4);
		
		FaktureRS fakture2 = new FaktureRS();
		fakture2.getFakture().add(faktura2);
		customers.put(new Long(428847801), fakture2);
    }
}
