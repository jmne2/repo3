package jmn.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "lista")
public class FaktureRS {

	private List<FakturaRS> faktura = new ArrayList<FakturaRS>();

	public FaktureRS() {
		super();
	}

	public List<FakturaRS> getFakture() {
		return faktura;
	}

	public void setFakture(List<FakturaRS> fakture) {
		this.faktura = fakture;
	}

}
