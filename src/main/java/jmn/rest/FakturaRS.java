package jmn.rest;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "faktura")
public class FakturaRS {
	
	private Long id;
	
	@NotNull
	private Date datumRacuna;
	
	@NotNull
	private Date datumValute;
	
	@NotNull
	private double vrednostRobe;
	
	@NotNull
	private double vrednostUsluga;
	
	@NotNull
	private double ukupnoRobaIUsluge;
	
	@NotNull
	private double ukupanRabat;
	
	@NotNull
	private double ukupanPorez;
	
	@NotNull
	@Size(min=3, max=3)
	private String oznakaValute;
	
	@NotNull
	private double ukIznos;
		
	@NotNull
	private double preostaliIznos;
	
	@NotNull
	@Size(min=1, max=55)
	private String nazivKupca;
	
	@NotNull
	@Size(min=1, max=55)
	private String adresaKupca;

	@NotNull
	@Size(min=9, max=9)
	private String pibKupca;
	
	@NotNull
	@Size(min=1, max=255)
	private String nazivDobavljaca;
	
	@NotNull
	@Size(min=1, max=255)
	private String adresaDobavljaca;
	
	@NotNull
	@Size(min=9, max=9)
	private String pibDobavljaca;
	
	@NotNull
	@Size(min=6, max=6)
	private Long brRacuna;
	
	@NotNull
	@Size(min=18, max=18)
	private String uplataNaRacun;

	private List<StavkaFaktureRS> products = new ArrayList<StavkaFaktureRS>();
	
	public FakturaRS() {
		
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getDatumRacuna() {
		return datumRacuna;
	}

	public void setDatumRacuna(Date datumRacuna) {
		this.datumRacuna = datumRacuna;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}

	public double getVrednostRobe() {
		return vrednostRobe;
	}

	public void setVrednostRobe(double vrednostRobe) {
		this.vrednostRobe = vrednostRobe;
	}

	public double getVrednostUsluga() {
		return vrednostUsluga;
	}

	public void setVrednostUsluga(double vrednostUsluga) {
		this.vrednostUsluga = vrednostUsluga;
	}

	public double getUkupnoRobaIUsluge() {
		return ukupnoRobaIUsluge;
	}

	public void setUkupnoRobaIUsluge(double ukupnoRobaIUsluge) {
		this.ukupnoRobaIUsluge = ukupnoRobaIUsluge;
	}

	public double getUkupanRabat() {
		return ukupanRabat;
	}

	public void setUkupanRabat(double ukupanRabat) {
		this.ukupanRabat = ukupanRabat;
	}

	public double getUkupanPorez() {
		return ukupanPorez;
	}

	public void setUkupanPorez(double ukupanPorez) {
		this.ukupanPorez = ukupanPorez;
	}

	public String getOznakaValute() {
		return oznakaValute;
	}

	public void setOznakaValute(String oznakaValute) {
		this.oznakaValute = oznakaValute;
	}

	public double getUkIznos() {
		return ukIznos;
	}

	public void setUkIznos(double ukIznos) {
		this.ukIznos = ukIznos;
	}

	public double getPreostaliIznos() {
		return preostaliIznos;
	}

	public void setPreostaliIznos(double preostaliIznos) {
		this.preostaliIznos = preostaliIznos;
	}

	public String getNazivKupca() {
		return nazivKupca;
	}

	public void setNazivKupca(String nazivKupca) {
		this.nazivKupca = nazivKupca;
	}

	public String getAdresaKupca() {
		return adresaKupca;
	}

	public void setAdresaKupca(String adresaKupca) {
		this.adresaKupca = adresaKupca;
	}

	public String getPibKupca() {
		return pibKupca;
	}

	public void setPibKupca(String pibKupca) {
		this.pibKupca = pibKupca;
	}

	public String getNazivDobavljaca() {
		return nazivDobavljaca;
	}

	public void setNazivDobavljaca(String nazivDobavljaca) {
		this.nazivDobavljaca = nazivDobavljaca;
	}

	public String getAdresaDobavljaca() {
		return adresaDobavljaca;
	}

	public void setAdresaDobavljaca(String adresaDobavljaca) {
		this.adresaDobavljaca = adresaDobavljaca;
	}

	public String getPibDobavljaca() {
		return pibDobavljaca;
	}

	public void setPibDobavljaca(String pibDobavljaca) {
		this.pibDobavljaca = pibDobavljaca;
	}

	public Long getBrRacuna() {
		return brRacuna;
	}

	public void setBrRacuna(Long brRacuna) {
		this.brRacuna = brRacuna;
	}

	@XmlElement(nillable = false)
	public String getUplataNaRacun() {
		return uplataNaRacun;
	}

	public void setUplataNaRacun(String uplataNaRacun) {
		this.uplataNaRacun = uplataNaRacun;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<StavkaFaktureRS> getProducts() {
		return products;
	}

	public void setProducts(List<StavkaFaktureRS> products) {
		this.products = products;
	}
}
