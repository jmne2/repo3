package jmn.repositories;

import org.springframework.data.repository.CrudRepository;

import jmn.models.User;

public interface UserRepository extends CrudRepository<User, Long>{

	public void delete(User us);
	
	public User findByUsernameAndPassword(String username, String password);
	
	public User findByUsername(String username);
}
