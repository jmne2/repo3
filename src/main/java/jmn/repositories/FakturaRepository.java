package jmn.repositories;

import java.util.List;
import java.sql.Date;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Faktura;
import jmn.models.PoslovnaGodina;
import jmn.models.PoslovniPartner;
import jmn.models.Preduzece;
import jmn.models.RacunPartnera;

public interface FakturaRepository extends CrudRepository<Faktura, Long>{

	public List<Faktura> findByBrojFaktureAndPoslovnaGodinaAndPoslovniPartner(Long brojFakture, PoslovnaGodina poslovnaGodina,
			PoslovniPartner poslovniPartner);
	public List<Faktura> findByBrojFaktureAndPoslovnaGodina(Long brojFakture, PoslovnaGodina poslovnaGodina);
	public List<Faktura> findByBrojFaktureAndPoslovniPartner(Long brojFakture, PoslovniPartner poslovniPartner);
	//za pronalazak ulaznih faktura nekog preduzeca
	public List<Faktura> findByVrstaFaktureAndPreduzece(String vrstaFakture, Preduzece preduzece);
	public List<Faktura> findByPreduzeceAndBrojFaktureAndVrstaFakture(Preduzece preduzece, Long brojFakture, String vrstaFakture);
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture, double preostaliIznos);
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndPreostaliIznosGreaterThan(Preduzece preduzece,String vrstaFakture,PoslovniPartner poslovniPartner, double preostaliIznos);
	public List<Faktura> findByBrojFaktureAndPreduzeceAndPoslovniPartnerAndRacunPartneraAndVrstaFakture(Long brojFakture, Preduzece preduzece, PoslovniPartner poslovniPartner, RacunPartnera racunPartnera, String vrstaFakture);
	public List<Faktura> findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFakture(PoslovniPartner poslovniPartner, Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture);
	public List<Faktura> findByPoslovniPartnerAndPreduzeceAndRacunPartneraAndVrstaFaktureAndDatumFakture(PoslovniPartner poslovniPartner, Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture, Date datumFakture);
	public List<Faktura> findByPreduzeceAndDatumFaktureAndVrstaFakture(Preduzece preduzece, Date datumFakture, String vrstaFakture);
	public Faktura findByBrojFaktureAndPreduzeceAndDatumFakture(Long brojFakture, Preduzece preduzece, Date datumFakture);
	public List<Faktura> findByPreduzeceAndRacunPartneraAndVrstaFakture(Preduzece preduzece, RacunPartnera racunPartnera, String vrstaFakture);
	public List<Faktura> findByPreduzeceAndVrstaFaktureAndPoslovniPartnerAndBrojFakture(Preduzece preduzece, String vrstaFakture,PoslovniPartner poslovniPartner, Long brojFakture);

}
