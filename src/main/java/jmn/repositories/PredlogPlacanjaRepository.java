package jmn.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.PredlogPlacanja;
import jmn.models.Preduzece;

public interface PredlogPlacanjaRepository extends CrudRepository<PredlogPlacanja, Long> {

	public List<PredlogPlacanja> findByDatumAndStatusContainingAndBrojContainingAndPreduzece(Date datum, String status,
			String broj, Preduzece preduzece);

	public List<PredlogPlacanja> findByStatusContainingAndBrojContainingAndPreduzece(String status, String broj, Preduzece preduzece);

	public List<PredlogPlacanja> findByPreduzece(Preduzece preduzece);
	
	public List<PredlogPlacanja> findByStatusAndPreduzece(String status, Preduzece preduzece);
}
