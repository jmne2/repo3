package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.Preduzece;

public interface PreduzeceRepository extends CrudRepository<Preduzece, Long>{

	public List<Preduzece> findByNazivContainingAndPib(String naziv, int pib);
	public Preduzece findByPib(int pib);
	public Preduzece findByMaticniBroj(Long maticniBroj);
}
