package jmn.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jmn.models.DnevnoStanje;
import jmn.models.PoslovniPartner;
import jmn.models.StavkaIzvoda;

public interface StavkaIzvodaRepository extends CrudRepository<StavkaIzvoda, Long>{

	public List<StavkaIzvoda> findByDnevnoStanje(DnevnoStanje dnevnoStanje);
	public List<StavkaIzvoda> findByDnevnoStanjeAndSmerIgnoreCase(DnevnoStanje dnevnoStanje, String smer);
}
