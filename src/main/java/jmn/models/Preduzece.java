package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Preduzece implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String naziv;
	
	@Column(nullable = false)
	private int pib;
	
	@Column(nullable = false)
	private Long maticniBroj;
	
	@Column(nullable = false)
	private Long brTelefona;
	
	@Column(nullable = true)
	private Long fax;
	
	@Column(nullable = false)
	private String email;
	
	@OneToOne(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="preduzece")
	@JsonIgnore
	private DEK dek;
	
	@ManyToOne()
	@JoinColumn(name="sifraDelatnosti")
	private SifrarnikDelatnosti sifraDelatnosti;
	
	@ManyToOne()
	@JoinColumn(name="adresa")
	private Adresa adresa;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="preduzece")
	@JsonIgnore
	private Set<PoslovnaGodina> poslovneGodine = new HashSet<PoslovnaGodina>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="preduzece")
	@JsonIgnore
	private Set<PoslovniPartner> poslovniPartneri = new HashSet<PoslovniPartner>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="preduzece")
	@JsonIgnore
	private Set<Racun> racuni = new HashSet<Racun>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="preduzece")
	@JsonIgnore
	private Set<Faktura> fakture = new HashSet<Faktura>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="preduzece")
	@JsonIgnore
	private Set<PredlogPlacanja> predlozi = new HashSet<PredlogPlacanja>();
	
	public Preduzece(){
		
	}

	public Preduzece(String naziv, int pib, Long maticniBroj, Long brTelefona, Long fax, String email,
			SifrarnikDelatnosti sifraDelatnosti, Adresa adresa, Set<PoslovnaGodina> poslovneGodine,
			Set<PoslovniPartner> poslovniPartneri, Set<Racun> racuni, Set<Faktura> fakture, Set<PredlogPlacanja> predlozi,
			DEK dek) {
		super();
		this.naziv = naziv;
		this.pib = pib;
		this.maticniBroj = maticniBroj;
		this.brTelefona = brTelefona;
		this.fax = fax;
		this.email = email;
		this.sifraDelatnosti = sifraDelatnosti;
		this.adresa = adresa;
		this.poslovneGodine = poslovneGodine;
		this.poslovniPartneri = poslovniPartneri;
		this.racuni = racuni;
		this.fakture = fakture;
		this.predlozi = predlozi;
		this.dek = dek;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getPib() {
		return pib;
	}

	public void setPib(int pib) {
		this.pib = pib;
	}

	public Long getMaticniBroj() {
		return maticniBroj;
	}

	public void setMaticniBroj(Long maticniBroj) {
		this.maticniBroj = maticniBroj;
	}

	public Long getBrTelefona() {
		return brTelefona;
	}

	public void setBrTelefona(Long brTelefona) {
		this.brTelefona = brTelefona;
	}

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SifrarnikDelatnosti getSifraDelatnosti() {
		return sifraDelatnosti;
	}

	public void setSifraDelatnosti(SifrarnikDelatnosti sifraDelatnosti) {
		this.sifraDelatnosti = sifraDelatnosti;
		if(!sifraDelatnosti.getPreduzeca().contains(this)){
			sifraDelatnosti.getPreduzeca().add(this);
		}
	}

	public Long getId() {
		return id;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
		if(adresa.getPreduzeca()==null || !adresa.getPreduzeca().contains(this)){
			adresa.addPreduzece(this);
		}
	}

	public Set<PoslovnaGodina> getPoslovneGodine() {
		return poslovneGodine;
	}

	public void setPoslovneGodine(Set<PoslovnaGodina> poslovneGodine) {
		this.poslovneGodine = poslovneGodine;
	}

	public void addPoslovnaGodina(PoslovnaGodina pg){
		this.poslovneGodine.add(pg);
		if(!pg.getPreduzece().equals(this))
			pg.setPreduzece(this);
	}
	
	public Set<PoslovniPartner> getPoslovniPartneri() {
		return poslovniPartneri;
	}

	public void setPoslovniPartneri(Set<PoslovniPartner> poslovniPartneri) {
		this.poslovniPartneri = poslovniPartneri;
	}
	
	public void addPoslovniPartner(PoslovniPartner pp){
		this.poslovniPartneri.add(pp);
		if(!pp.getPreduzece().equals(this))
			pp.setPreduzece(this);
	}

	public Set<Racun> getRacuni() {
		return racuni;
	}

	public void setRacuni(Set<Racun> racuni) {
		this.racuni = racuni;
	}
	
	public void addRacun(Racun r){
		this.racuni.add(r);
		if(!r.getPreduzece().equals(this))
			r.setPreduzece(this);
	}

	public Set<Faktura> getFakture() {
		return fakture;
	}

	public void setFakture(Set<Faktura> fakture) {
		this.fakture = fakture;
	}
	
	public void addFaktura(Faktura f){
		this.fakture.add(f);
		if(!f.getPreduzece().equals(this))
			f.setPreduzece(this);
	}

	public Set<PredlogPlacanja> getPredlozi() {
		return predlozi;
	}

	public void setPredlozi(Set<PredlogPlacanja> predlozi) {
		this.predlozi = predlozi;
	}
	
	public void addPredlog(PredlogPlacanja p){
		this.predlozi.add(p);
		if(!p.getPreduzece().equals(this))
			p.setPreduzece(this);
	}

	public DEK getDek() {
		return dek;
	}

	public void setDek(DEK dek) {
		this.dek = dek;
	}
	
}
