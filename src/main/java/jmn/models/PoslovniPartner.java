package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PoslovniPartner implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String naziv;
	
	@Column(nullable = false)
	private int pib;
	
	@Column(nullable = false)
	private Long maticniBroj;
	
	@Column(nullable = false)
	private Long brTelefona;
	
	@Column(nullable = true)
	private Long fax;
	
	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String vrsta;
	
	@ManyToOne()
	@JoinColumn(name="sifraDelatnosti")
	private SifrarnikDelatnosti sifraDelatnosti;
	
	@ManyToOne()
	@JoinColumn(name="adresa")
	private Adresa adresa;
		
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="poslovniPartner")
	@JsonIgnore
	private Set<Faktura> fakture = new HashSet<Faktura>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="poslovniPartner")
	@JsonIgnore
	private Set<RacunPartnera> racuniPartnera = new HashSet<RacunPartnera>();
		
	@ManyToOne()
	@JoinColumn(name="preduzece")
	private Preduzece preduzece;
	
	public PoslovniPartner(){
		
	}

	public PoslovniPartner(String naziv, int pib, Long maticniBroj, Long brTelefona, Long fax, String email,
			String vrsta, SifrarnikDelatnosti sifraDelatnosti, Adresa adresa, Set<Faktura> fakture,
			 Set<RacunPartnera> racuniPartnera,
			Preduzece preduzece) {
		super();
		this.naziv = naziv;
		this.pib = pib;
		this.maticniBroj = maticniBroj;
		this.brTelefona = brTelefona;
		this.fax = fax;
		this.email = email;
		this.vrsta = vrsta;
		this.sifraDelatnosti = sifraDelatnosti;
		this.adresa = adresa;
		this.fakture = fakture;
		this.racuniPartnera = racuniPartnera;
		this.preduzece = preduzece;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getPib() {
		return pib;
	}

	public void setPib(int pib) {
		this.pib = pib;
	}

	public Long getMaticniBroj() {
		return maticniBroj;
	}

	public void setMaticniBroj(Long maticniBroj) {
		this.maticniBroj = maticniBroj;
	}

	public Long getBrTelefona() {
		return brTelefona;
	}

	public void setBrTelefona(Long brTelefona) {
		this.brTelefona = brTelefona;
	}

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SifrarnikDelatnosti getSifraDelatnosti() {
		return sifraDelatnosti;
	}

	public void setSifraDelatnosti(SifrarnikDelatnosti sifraDelatnosti) {
		this.sifraDelatnosti = sifraDelatnosti;
		if(!sifraDelatnosti.getPoslovniPartneri().contains(this)){
			sifraDelatnosti.getPoslovniPartneri().add(this);
		}
	}

	public Long getId() {
		return id;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
		if(adresa.getPoslovniPartneri()==null || !adresa.getPoslovniPartneri().contains(this)){
			adresa.addPoslovniPartner(this);
		}
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public Set<Faktura> getFakture() {
		return fakture;
	}

	public void setFakture(Set<Faktura> fakture) {
		this.fakture = fakture;
	}

	public void addFaktura(Faktura f){
		this.fakture.add(f);
		if(!f.getPoslovniPartner().equals(this))
			f.setPoslovniPartner(this);
	}
	
	
	public Set<RacunPartnera> getRacuniPartnera() {
		return racuniPartnera;
	}

	public void setRacuniPartnera(Set<RacunPartnera> racuniPartnera) {
		this.racuniPartnera = racuniPartnera;
	}

	public void addRacunPartnera(RacunPartnera rp){
		this.racuniPartnera.add(rp);
		if(!rp.getPoslovniPartner().equals(this))
			rp.setPoslovniPartner(this);
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}
	
}

