package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Racun implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String brRacuna;

	@ManyToOne()
	@JoinColumn(name="banka")
	private Banka banka;
	
	@ManyToOne()
	@JoinColumn(name="preduzece")
	private Preduzece preduzece;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="racun")
	@JsonIgnore
	private Set<DnevnoStanje> dnevnaStanja = new HashSet<DnevnoStanje>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="racunUplatioca")
	@JsonIgnore
	private Set<NalogZaPlacanje> nalozi = new HashSet<NalogZaPlacanje>();
	
	public Racun() {
	}

	public Racun(String brRacuna, Banka banka, Preduzece preduzece, Set<DnevnoStanje> dnevnaStanja, Set<NalogZaPlacanje> nalozi) {
		super();
		this.brRacuna = brRacuna;
		this.banka = banka;
		this.preduzece = preduzece;
		this.dnevnaStanja = dnevnaStanja;
		this.nalozi = nalozi;
	}

	public String getBrRacuna() {
		return brRacuna;
	}

	public void setBrRacuna(String brRacuna) {
		this.brRacuna = brRacuna;
	}

	public Banka getBanka() {
		return banka;
	}

	public void setBanka(Banka banka) {
		this.banka = banka;
	}

	public Preduzece getPreduzece() {
		return preduzece;
	}

	public void setPreduzece(Preduzece preduzece) {
		this.preduzece = preduzece;
	}

	public Set<DnevnoStanje> getDnevnaStanja() {
		return dnevnaStanja;
	}

	public void setDnevnaStanja(Set<DnevnoStanje> dnevnaStanja) {
		this.dnevnaStanja = dnevnaStanja;
	}

	public void addDnevnoStanje(DnevnoStanje ds){
		this.dnevnaStanja.add(ds);
		if(!ds.getRacun().equals(this))
			ds.setRacun(this);
	}
	
	public Long getId() {
		return id;
	}

	public Set<NalogZaPlacanje> getNalozi() {
		return nalozi;
	}

	public void setNalozi(Set<NalogZaPlacanje> nalozi) {
		this.nalozi = nalozi;
	}
	
}
