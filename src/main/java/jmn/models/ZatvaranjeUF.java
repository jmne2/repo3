package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ZatvaranjeUF implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private double iznos;
	
	@Column(nullable = false)
	private Date datumIzvoda;
	
	@ManyToOne()
	@JoinColumn(name="ulaznaFaktura")
	private Faktura ulaznaFaktura;
	
	@ManyToOne()
	@JoinColumn(name="stavkaIzvoda")
	private StavkaIzvoda stavkaIzvoda;

	public ZatvaranjeUF() {
	}

	public ZatvaranjeUF(double iznos, Date datumIzvoda, Faktura ulaznaFaktura, StavkaIzvoda stavkaIzvoda) {
		super();
		this.iznos = iznos;
		this.datumIzvoda = datumIzvoda;
		this.ulaznaFaktura = ulaznaFaktura;
		this.stavkaIzvoda = stavkaIzvoda;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public Faktura getFaktura() {
		return ulaznaFaktura;
	}

	public void setFaktura(Faktura ulaznaFaktura) {
		this.ulaznaFaktura = ulaznaFaktura;
	}

	public StavkaIzvoda getStavkaIzvoda() {
		return stavkaIzvoda;
	}

	public void setStavkaIzvoda(StavkaIzvoda stavkaIzvoda) {
		this.stavkaIzvoda = stavkaIzvoda;
	}

	public Long getId() {
		return id;
	}

	public Date getDatumIzvoda() {
		return datumIzvoda;
	}

	public void setDatumIzvoda(Date datumIzvoda) {
		this.datumIzvoda = datumIzvoda;
	}	
}
