package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class SifrarnikDelatnosti implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String sifra;
	
	@Column(nullable = false)
	private String nazivDelatnosti;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="sifraDelatnosti")
	@JsonIgnore
	private Set<Preduzece> preduzeca = new HashSet<Preduzece>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="sifraDelatnosti")
	@JsonIgnore
	private Set<PoslovniPartner> poslovniPartneri = new HashSet<PoslovniPartner>();
	
	public SifrarnikDelatnosti(){
		
	}

	public SifrarnikDelatnosti(String sifra, String nazivDelatnosti, Set<Preduzece> preduzeca,
			Set<PoslovniPartner> poslovniPartneri) {
		super();
		this.sifra = sifra;
		this.nazivDelatnosti = nazivDelatnosti;
		this.preduzeca = preduzeca;
		this.poslovniPartneri = poslovniPartneri;
	}


	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getNazivDelatnosti() {
		return nazivDelatnosti;
	}

	public void setNazivDelatnosti(String nazivDelatnosti) {
		this.nazivDelatnosti = nazivDelatnosti;
	}

	public Set<Preduzece> getPreduzeca() {
		return preduzeca;
	}

	public void setPreduzeca(Set<Preduzece> preduzeca) {
		this.preduzeca = preduzeca;
	}

	public Long getId() {
		return id;
	}
	
	public void addPreduzece(Preduzece preduzece){
		this.preduzeca.add(preduzece);
		if(!preduzece.getSifraDelatnosti().getSifra().equals(this.sifra)){
			preduzece.setSifraDelatnosti(this);
		}
	}
	
	public Set<PoslovniPartner> getPoslovniPartneri() {
		return poslovniPartneri;
	}

	public void setPoslovniPartneri(Set<PoslovniPartner> poslovniPartneri) {
		this.poslovniPartneri = poslovniPartneri;
	}
	
	public void addPoslovniPartner(PoslovniPartner pp){
		this.poslovniPartneri.add(pp);
		if(!pp.getSifraDelatnosti().getSifra().equals(this.sifra)){
			pp.setSifraDelatnosti(this);
		}
	}
}
