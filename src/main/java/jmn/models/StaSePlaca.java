package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class StaSePlaca implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private double iznos;
	
	@Column(nullable = false)
	private String saKogRacuna;
	
	@ManyToOne()
	@JoinColumn(name="faktura")
	private Faktura faktura;

	@ManyToOne()
	@JoinColumn(name="predlogPlacanja")
	private PredlogPlacanja predlogPlacanja;
	

	public StaSePlaca() {
	}

	public StaSePlaca(double iznos, String saKogRacuna, Faktura faktura, PredlogPlacanja predlogPlacanja) {
		super();
		this.iznos = iznos;
		this.saKogRacuna = saKogRacuna;
		this.faktura = faktura;
		this.predlogPlacanja = predlogPlacanja;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public String getSaKogRacuna() {
		return saKogRacuna;
	}

	public void setSaKogRacuna(String saKogRacuna) {
		this.saKogRacuna = saKogRacuna;
	}

	public Faktura getFaktura() {
		return faktura;
	}

	public void setUlaznaFaktura(Faktura ulaznaFaktura) {
		this.faktura = ulaznaFaktura;
		if(ulaznaFaktura.getListaStaSePlaca()==null || !ulaznaFaktura.getListaStaSePlaca().contains(this)){
			ulaznaFaktura.addStaSePlaca(this);
		}
	}

	public Long getId() {
		return id;
	}

	public PredlogPlacanja getPredlogPlacanja() {
		return predlogPlacanja;
	}

	public void setPredlogPlacanja(PredlogPlacanja predlogPlacanja) {
		this.predlogPlacanja = predlogPlacanja;
		if(predlogPlacanja.getListaStaSePlaca()==null || !predlogPlacanja.getListaStaSePlaca().contains(this)){
			predlogPlacanja.addStaSePlaca(this);
		}
	}
	
	
	
}
