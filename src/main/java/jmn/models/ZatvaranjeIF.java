package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ZatvaranjeIF implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private double iznos;
	
	@Column(nullable = false)
	private Date datumIzvoda;
	
	@ManyToOne()
	@JoinColumn(name="izlaznaFaktura")
	private Faktura izlaznaFaktura;
	
	@ManyToOne()
	@JoinColumn(name="stavkaIzvoda")
	private StavkaIzvoda stavkaIzvoda;
	
	public ZatvaranjeIF() {
	}

	public ZatvaranjeIF(double iznos, Date datumIznosa, Faktura izlaznaFaktura, StavkaIzvoda stavkaIzvoda) {
		super();
		this.iznos = iznos;
		this.datumIzvoda = datumIznosa;
		this.izlaznaFaktura = izlaznaFaktura;
		this.stavkaIzvoda = stavkaIzvoda;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public Faktura getFaktura() {
		return izlaznaFaktura;
	}



	public void setFaktura(Faktura izlaznaFaktura) {
		this.izlaznaFaktura = izlaznaFaktura;
	}

	public StavkaIzvoda getStavkaIzvoda() {
		return stavkaIzvoda;
	}

	public void setStavkaIzvoda(StavkaIzvoda stavkaIzvoda) {
		this.stavkaIzvoda = stavkaIzvoda;
	}

	public Long getId() {
		return id;
	}

	public Date getDatumIzvoda() {
		return datumIzvoda;
	}

	public void setDatumIzvoda(Date datumIzvoda) {
		this.datumIzvoda = datumIzvoda;
	}	
}
