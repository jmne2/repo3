package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Adresa implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column (nullable=false)
	private String ulica;
	
	@Column (nullable = false)
	private int broj;
	
	@ManyToOne()
	@JoinColumn(name="nasMesto")
	private NaseljenoMesto nasMesto;
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="adresa")
	@JsonIgnore
	private Set<Preduzece> preduzeca = new HashSet<Preduzece>();
	
	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="adresa")
	@JsonIgnore
	private Set<PoslovniPartner> poslovniPartneri = new HashSet<PoslovniPartner>();
	
	public Adresa(){
		
	}
	
	public Adresa(String ulica, int broj, NaseljenoMesto nasMesto) {
		super();
		this.ulica = ulica;
		this.broj = broj;
		this.nasMesto = nasMesto;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public int getBroj() {
		return broj;
	}

	public void setBroj(int broj) {
		this.broj = broj;
	}

	public NaseljenoMesto getNasMesto() {
		return nasMesto;
	}

	public void setNasMesto(NaseljenoMesto nasMesto) {
		this.nasMesto = nasMesto;
		if(!nasMesto.getAdrese().contains(this)){
			nasMesto.getAdrese().add(this);
		}
	}

	public Long getId() {
		return id;
	}

	public Set<Preduzece> getPreduzeca() {
		return preduzeca;
	}

	public void setPreduzeca(Set<Preduzece> preduzeca) {
		this.preduzeca = preduzeca;
	}
	
	public void addPreduzece(Preduzece pred){
		this.preduzeca.add(pred);
		if(!pred.getAdresa().equals(this)){
			pred.setAdresa(this);
		}
	}

	public Set<PoslovniPartner> getPoslovniPartneri() {
		return poslovniPartneri;
	}

	public void setPoslovniPartneri(Set<PoslovniPartner> poslovniPartneri) {
		this.poslovniPartneri = poslovniPartneri;
	}
	
	public void addPoslovniPartner(PoslovniPartner pp){
		this.poslovniPartneri.add(pp);
		if(!pp.getAdresa().equals(this)){
			pp.setAdresa(this);
		}
	}
	
}
