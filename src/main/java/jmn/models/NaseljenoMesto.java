package jmn.models;

import static javax.persistence.CascadeType.ALL;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class NaseljenoMesto implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String naziv;
	
	@Column(nullable = false)
	private String postBroj;
	
	@ManyToOne()
	@JoinColumn(name="drzava")
	//@JsonBackReference
	private Drzava drzava;

	@OneToMany(cascade={ALL}, fetch=FetchType.LAZY, mappedBy="nasMesto")
	@JsonIgnore
	private Set<Adresa> adrese = new HashSet<Adresa>();
	
	public NaseljenoMesto(){
	}
	
	public NaseljenoMesto(String naziv, String postBroj, Drzava drzava) {
		super();
		this.naziv = naziv;
		this.postBroj = postBroj;
		this.drzava = drzava;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPostBroj() {
		return postBroj;
	}

	public void setPostBroj(String postBroj) {
		this.postBroj = postBroj;
	}

	public Drzava getDrzava() {
		return drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
		if(!drzava.getNasMesta().contains(this)){
			drzava.addNasMesto(this);
		}
	}

	public Long getId() {
		return id;
	}

	public Set<Adresa> getAdrese() {
		return adrese;
	}

	public void setAdrese(Set<Adresa> adresa) {
		this.adrese = adresa;
	}
	
	public void addAdresa(Adresa adresa){
		this.adrese.add(adresa);
		if(adresa.getNasMesto()==null || !adresa.getNasMesto().getNaziv().equals(this.naziv)){
			adresa.setNasMesto(this);
		}
	}
	
}
