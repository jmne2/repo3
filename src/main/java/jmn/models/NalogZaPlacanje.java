package jmn.models;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class NalogZaPlacanje implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false)
	private String uplatilac;
	
	@ManyToOne()
	@JoinColumn(name="racunUplatioca")
	private Racun racunUplatioca;
	
	@Column(nullable = false)
	private  String svrhaUplate;
	
	@Column(nullable = false)
	private String primalac;
	
	@Column(nullable = false)
	private String sifraPlacanja;
	
	@Column(nullable = false)
	private String valuta;
	
	@Column(nullable = false)
	private double iznos;
	
	@ManyToOne()
	@JoinColumn(name="racunPrimaoca")
	private RacunPartnera racunPrimaoca;

	@Column(nullable = false)
	private int brModela;
	
	@Column(nullable = false)
	private int brModelaOdobrenja;
		
	@Column(nullable = false)
	private String pozivNaBroj;

	@Column(nullable = false)
	private String pozivNaBrojOdobrenja;
	
	@ManyToOne()
	@JoinColumn(name="zaPredlog")
	private PredlogPlacanja zaPredlog;
	
	@Column(nullable = false)
	private boolean hitno;
	
	public NalogZaPlacanje() {
	}

	public NalogZaPlacanje(String uplatilac, Racun racunUplatioca, String svrhaUplate, String primalac,
			String sifraPlacanja, String valuta, double iznos, RacunPartnera racunPrimaoca, int brModela,
			int brModelaOdobrenja, String pozivNaBroj, String pozivNaBrojOdobrenja, PredlogPlacanja zaPredlog,
			boolean hitno) {
		super();
		this.uplatilac = uplatilac;
		this.racunUplatioca = racunUplatioca;
		this.svrhaUplate = svrhaUplate;
		this.primalac = primalac;
		this.sifraPlacanja = sifraPlacanja;
		this.valuta = valuta;
		this.iznos = iznos;
		this.racunPrimaoca = racunPrimaoca;
		this.brModela = brModela;
		this.brModelaOdobrenja = brModelaOdobrenja;
		this.pozivNaBroj = pozivNaBroj;
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
		this.zaPredlog = zaPredlog;
		this.hitno = hitno;
	}


	public String getUplatilac() {
		return uplatilac;
	}

	public void setUplatilac(String uplatilac) {
		this.uplatilac = uplatilac;
	}

	public String getSvrhaUplate() {
		return svrhaUplate;
	}

	public void setSvrhaUplate(String svrhaUplate) {
		this.svrhaUplate = svrhaUplate;
	}

	public String getPrimalac() {
		return primalac;
	}

	public void setPrimalac(String primalac) {
		this.primalac = primalac;
	}

	public String getSifraPlacanja() {
		return sifraPlacanja;
	}

	public void setSifraPlacanja(String sifraPlacanja) {
		this.sifraPlacanja = sifraPlacanja;
	}

	public String getValuta() {
		return valuta;
	}

	public void setValuta(String valuta) {
		this.valuta = valuta;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public RacunPartnera getRacunPrimaoca() {
		return racunPrimaoca;
	}

	public void setRacunPrimaoca(RacunPartnera racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}

	public int getBrModela() {
		return brModela;
	}

	public void setBrModela(int brModela) {
		this.brModela = brModela;
	}

	public String getPozivNaBroj() {
		return pozivNaBroj;
	}

	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}

	public Long getId() {
		return id;
	}

	public PredlogPlacanja getZaPredlog() {
		return zaPredlog;
	}

	public void setZaPredlog(PredlogPlacanja zaPredlog) {
		this.zaPredlog = zaPredlog;
	}

	public Racun getRacunUplatioca() {
		return racunUplatioca;
	}

	public void setRacunUplatioca(Racun racunUplatioca) {
		this.racunUplatioca = racunUplatioca;
	}

	public boolean isHitno() {
		return hitno;
	}

	public void setHitno(boolean hitno) {
		this.hitno = hitno;
	}

	public int getBrModelaOdobrenja() {
		return brModelaOdobrenja;
	}

	public void setBrModelaOdobrenja(int brModelaOdobrenja) {
		this.brModelaOdobrenja = brModelaOdobrenja;
	}

	public String getPozivNaBrojOdobrenja() {
		return pozivNaBrojOdobrenja;
	}

	public void setPozivNaBrojOdobrenja(String pozivNaBrojOdobrenja) {
		this.pozivNaBrojOdobrenja = pozivNaBrojOdobrenja;
	}
}
