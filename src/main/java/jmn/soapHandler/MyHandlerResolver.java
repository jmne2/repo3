package jmn.soapHandler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

public class MyHandlerResolver implements HandlerResolver {


    /**
     * Overrode in order to load custom handlers.
     * @see javax.xml.ws.handler.HandlerResolver#getHandlerChain(javax.xml.ws.handler.PortInfo)
     */
    public List<Handler> getHandlerChain(PortInfo portInfo) {
        List<Handler> handlerChain = new ArrayList<Handler>();
        MySoapHandler hh = new MySoapHandler();
        handlerChain.add(hh);
        return handlerChain;
    }
}