
'use strict';
 
angular.module('repo3App').factory('PoslovniPartnerService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    //var SERVICE_URI = 'http://localhost:8080/drzave/';
 
    var factory = {
        fetchAllPartnere: fetchAllPartnere,
        createPartnera: createPartnera,
        updatePartnera:updatePartnera,
        deletePartnera:deletePartnera,
        searchPartnere: searchPartnere
    };
 
    return factory;
 
    function fetchAllPartnere() {
        var deferred = $q.defer();
        $http.get(urls.PARTNER_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Partners');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createPartnera(partner) {
        var deferred = $q.defer();
        $http.post(urls.PARTNER_SERVICE, partner)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Partner');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updatePartnera(partner, id) {
        var deferred = $q.defer();
        $http.post(urls.PARTNER_SERVICE+id, partner)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deletePartnera(id) {
        var deferred = $q.defer();
        $http.delete(urls.PARTNER_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchPartnere(partner){
    	var deferred = $q.defer();
        $http.post(urls.PARTNER_SERVICE+"search/", partner)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);