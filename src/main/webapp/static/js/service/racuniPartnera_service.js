
'use strict';
 
angular.module('repo3App').factory('RacunPartneraService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    var factory = {
        fetchAllRacune: fetchAllRacune,
        createRacun: createRacun,
        updateRacun:updateRacun,
        deleteRacun:deleteRacun,
        searchRacune: searchRacune
    };
 
    return factory;
 
    function fetchAllRacune() {
        var deferred = $q.defer();
        $http.get(urls.RACUNPARTN_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Reciepts');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createRacun(racun) {
        var deferred = $q.defer();
        $http.post(urls.RACUNPARTN_SERVICE, racun)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Reciept');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateRacun(racun, id) {
        var deferred = $q.defer();
        $http.post(urls.RACUNPARTN_SERVICE+id, racun)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteRacun(id) {
        var deferred = $q.defer();
        $http.delete(urls.RACUNPARTN_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchRacune(racun){
    	var deferred = $q.defer();
        $http.post(urls.RACUNPARTN_SERVICE+"search/", racun)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);