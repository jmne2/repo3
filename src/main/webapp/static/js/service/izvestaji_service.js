'use strict';
 
angular.module('repo3App').factory('IzvestajiService', ['$http', '$q', 'urls', function($http, $q, urls){
 
 
    var factory = {
        fetchKUF: fetchKUF,
        fetchKUFPDF: fetchKUFPDF,
        fetchKIF: fetchKIF,
        fetchKarticePartnera: fetchKarticePartnera,
        fetchIOS : fetchIOS,
        fetchPartneri: fetchPartneri,
        fetchKupce: fetchKupce
    };
 
    return factory;
 
    function fetchKUF(datumi) {
        var deferred = $q.defer();
        $http.post(urls.IZVESTAJI_SERVICE+"KUF/", datumi)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching KUF');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function fetchKUFPDF(parametar) {
        var deferred = $q.defer();
        $http.get(urls.IZVESTAJI_SERVICE+"KUF/getPdf/",parametar)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching PDF');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function fetchKIF(datumDonji) {
        var deferred = $q.defer();
        $http.get(urls.IZVESTAJI_SERVICE+"KIF/",datumDonji)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching KIF');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 

    function fetchKarticePartnera(kupac){
    	var deferred = $q.defer();
        $http.post(urls.IZVESTAJI_SERVICE+"partneri/", kupac)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function fetchKupce(){
    	var deferred = $q.defer();
        $http.get(urls.IZVESTAJI_SERVICE+"kupci/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function fetchPartneri(){
    	var deferred = $q.defer();
        $http.get(urls.IZVESTAJI_SERVICE+"sviPartneri/")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function fetchIOS(partner){
    	var deferred = $q.defer();
        $http.post(urls.IZVESTAJI_SERVICE+"ios/", partner)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}]);