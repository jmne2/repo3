
'use strict';
 
angular.module('repo3App').factory('NaseljenoMestoService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    //var SERVICE_URI = 'http://localhost:8080/drzave/';
 
    var factory = {
        fetchAllMesta: fetchAllMesta,
        createMesto: createMesto,
        updateMesto: updateMesto,
        deleteMesto: deleteMesto,
        searchMesta: searchMesta
    };
 
    return factory;
 
    function fetchAllMesta() {
        var deferred = $q.defer();
        $http.get(urls.MESTO_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createMesto(drzava) {
        var deferred = $q.defer();
        $http.post(urls.MESTO_SERVICE, drzava)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateMesto(drzava, id) {
        var deferred = $q.defer();
        $http.post(urls.MESTO_SERVICE+id, drzava)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteMesto(id) {
        var deferred = $q.defer();
        $http.delete(urls.MESTO_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchMesta(mesto){
    	var deferred = $q.defer();
        $http.post(urls.MESTO_SERVICE+'search/', mesto)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);