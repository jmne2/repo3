
'use strict';
 
angular.module('repo3App').factory('SifrarnikDelatnostiService', ['$http', '$q', 'urls', function($http, $q, urls){
 
    //var SERVICE_URI = 'http://localhost:8080/drzave/';
 
    var factory = {
        fetchDelatnosti: fetchDelatnosti,
        createDelatnost: createDelatnost,
        updateDelatnost:updateDelatnost,
        deleteDelatnost:deleteDelatnost,
        searchDelatnost: searchDelatnost
    };
 
    return factory;
 
    function fetchDelatnosti() {
        var deferred = $q.defer();
        $http.get(urls.SIFRARNIK_DELATNOSTI_SERVICE)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function createDelatnost(delatnost) {
        var deferred = $q.defer();
        $http.post(urls.SIFRARNIK_DELATNOSTI_SERVICE, delatnost)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
 
    function updateDelatnost(delatnost, id) {
        var deferred = $q.defer();
        $http.post(urls.SIFRARNIK_DELATNOSTI_SERVICE+id, delatnost)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
    function deleteDelatnost(id) {
        var deferred = $q.defer();
        $http.delete(urls.SIFRARNIK_DELATNOSTI_SERVICE+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function searchDelatnost(delatnost){
    	var deferred = $q.defer();
        $http.post(urls.SIFRARNIK_DELATNOSTI_SERVICE+"search/", delatnost)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
 
}]);