'use strict';

angular.module('repo3App').controller('StaSePlacaController', ['$scope','$uibModal', 'PlacanjeService', 
                                                                    function($scope,$uibModal,PlacanjeService) {
    var self = this;
    self.placam={};
    self.placanje=[];
    self.racun = {};
    self.racuni = [];
    self.faktura = {};
    self.fakture = [];
    self.predlog = {};
    self.predlozi = [];
    self.show = false;
    self.logged = false;
    self.children = [""];
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showFakture = showFakture;
    self.showRacune = showRacune;
    self.showPredloge = showPredloge;
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		if(args.parent != null){
	    		if(!angular.isUndefined(args.parent.predlozi) && args.parent.predlozi != null){
	    			self.placam.zaPreglod = args.parent.predlozi[args.parent.indSelected];
	    			search(self.placam);
	    		} else if (!angular.isUndefined(args.parent.ufakture) && args.parent.ufakture != null){
	    			if(args.parent.ufaktureshow){
	    				self.placam.faktura = args.parent.ufaktura;
	    			}else if(args.parent.ifaktureshow){
	    				self.placam.faktura = args.parent.ifaktura;
	    			}
	    			search(self.placam);
	    		}
    		} else {
    			fetchAllPlacanje();	
    		}
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.placam = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllPlacanje();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.placanje[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.placanje.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.placanje.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.placanje[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showFakture() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.fakture;
            },
            type: function(){
          	  return "Fakturu";
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.faktura = self.fakture[selectedItem];
        }, function () {
        });
      };

      function showRacune() {

          var modalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html',
            controller: ModalInstanceCtrl,
            resolve: {
              items: function () {
                return self.racuni;
              },
              type: function(){
            	  return "Racun";
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            self.racun = self.racuni[selectedItem];
          }, function () {
          });
        };

        function showPredloge() {

            var modalInstance = $uibModal.open({
              templateUrl: 'myModalContent.html',
              controller: ModalInstanceCtrl,
              resolve: {
                items: function () {
                  return self.predlozi;
                },
                type: function(){
              	  return "Predlog";
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
              self.predlog = self.predlozi[selectedItem];
            }, function () {
            });
          };

    function fetchAllPlacanje(){
    	reset();
    	PlacanjeService.fetchAllPlacanje()
            .then(
            function(d) {
            	self.placanje = [];
                self.racuni = d["racuni"];
                self.predlozi = d["predlozi"];
                self.fakture = d["fakture"];
                self.placanje = d["placanje"];
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }
 
    function createPlacanje(partner){
    	PlacanjeService.createPlacanje(partner)
            .then(
            fetchAllPlacanje,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updatePlacanje(partner, id){
    	PlacanjeService.updatePlacanje(partner, id)
            .then(
            fetchAllPlacanje,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deletePlacanje(id){
    	PlacanjeService.deletePlacanje(id)
            .then(
            fetchAllPlacanje,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.faktura.id != null && !angular.isUndefined(self.faktura.id) && self.racun.id != null 
    				&& !angular.isUndefined(self.racun.id) && self.predlog.id != null	
    				&& !angular.isUndefined(self.predlog.id)){
    			self.placam.saKogRacuna = self.racun.brRacuna;
	            self.placam.faktura = self.faktura;
	            self.placam.predlogPlacanja = self.predlog;
	            createPlacanje(self.placam);
    		}
        }else if (self.currState === "edit"){
        	if(self.faktura.id != null && !angular.isUndefined(self.faktura.id) && self.racun.id != null 
    				&& !angular.isUndefined(self.racun.id) && self.predlog.id != null	
    				&& !angular.isUndefined(self.predlog.id)){
        		self.placam.saKogRacuna = self.racun.brRacuna;
        		self.placam.faktura = self.faktura;
        		self.placam.predlogPlacanja = self.predlog;
	            updatePlacanje(self.placam, self.placam.id);
        	}
        } else if (self.currState === "search"){
        	self.placam.saKogRacuna = self.racun.brRacuna;
        	self.placam.faktura = self.faktura;
        	self.placam.predlogPlacanja = self.predlog;
        	search(self.placam);
        }
    }
 
    function racunZaBr(br){
    	angular.forEach(self.racuni, function(value, index){
			if(value.brRacuna === br){
				self.racun = value;
				return false;
			}
		});
    }
    
    function edit(id){
        for(var i = 0; i < self.placanje.length; i++){
            if(self.placanje[i].id === id) {
                self.placam = angular.copy(self.placanje[i]);
                self.faktura = self.placam.faktura;
                racunZaBr(self.placam.saKogRacuna);
                self.predlog = self.placam.predlogPlacanja;
                break;
            }
        }
    }
 
    function remove(id){
        if(self.placam.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deletePlacanje(id);
    }
 
 
    function reset(){
        self.placam={};
        self.racun= {};
        self.predlog= {};
        self.faktura={};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(partner){
    	PlacanjeService.searchPlacanje(partner)
        .then(
        function(d) {
        	self.placanje = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, type) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };
    	  $scope.type = type;

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);