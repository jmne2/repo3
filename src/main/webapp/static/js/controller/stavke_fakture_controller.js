'use strict';

angular.module('repo3App').controller('StavkeFaktureController', ['$scope', 'StavkeFaktureService', function($scope,StavkeFaktureService) {
	var self = this;
    self.stavkaFakture={};
    self.stavkeFakture=[];
    self.faktura = {};
    self.show = false;
    self.logged = false;
    self.children = [""];
    
    self.submit = submit;
    self.edit = edit;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('hidingYoCountries', function(event, args) {
    	fetchAllAdrese();
    	self.show = args.arg[1];
    	self.currState = "edit";
    });
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		if(args.parent != null){
    			if(args.parent.ufaktureshow){
    				self.faktura = args.parent.ufaktura;
    			}else if(args.parent.ifaktureshow){
    				self.faktura = args.parent.ifaktura;
    			}
    			fetchAllStavkeFakture(self.faktura);
    		} 
    		
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		if(self.currState === "add"){
    		self.currState = "edit";
    		}
    		$scope.required = (self.currState === "search") ? false : true;
    		self.stavkaFakture = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllStavkeFakture();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.stavkeFakture.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.stavkeFakture.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.stavkeFakture[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    
    function fetchAllStavkeFakture(){
    	StavkeFaktureService.fetchAllStavkeFakture(self.faktura)
            .then(
            function(d) {
            	self.stavkeFakture = d;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
 
    

 
    function submit() {
    	if (self.currState === "add"){
        }else if (self.currState === "edit"){
        } else if (self.currState === "search"){
        	search(self.stavkaFakture);
        }
	        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.stavkeFakture.length; i++){
            if(self.stavkeFakture[i].id === id) {
                self.stavkaFakture = angular.copy(self.stavkeFakture[i]);
                break;
            }
        }
    }
 

 
 
    function reset(){
    	self.stavkaFakture={};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(stavkaFakture){
    	StavkeFaktureService.searchStavkeFakture(stavkaFakture)
        .then(
        function(d) {
        	self.stavkeFakture = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching stavke fakture');
        }
        );
    }
    
}]);