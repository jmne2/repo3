'use strict';

angular.module('repo3App').controller('PoslovniPartnerController', ['$scope','$uibModal', 'PoslovniPartnerService', 
                                                                    function($scope,$uibModal,PoslovniPartnerService) {
    var self = this;
    self.adresa={id:null};
    self.adrese=[];
    self.preduzece = {};
    self.preduzeca = [];
    self.partner = {};
    self.partneri = [];
    self.delatnost = {};
    self.delatnosti = [];
    self.show = false;
    self.logged = false;
    self.children = [""];
    self.vrste = ["prodaje", "kupuje"];
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showAdreses = showAdreses;
    self.showWorks = showWorks;
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		self.preduzece = args.info.preduzece;
    		fetchAllPartnere();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.partner = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllPartnere();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.partneri[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.partneri.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.partneri.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.partneri[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showAdreses() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.adrese;
            },
            type: function(){
          	  return "Adresu";
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.adresa = self.adrese[selectedItem];
        }, function () {
        });
      };
/*
      function showCorps() {

          var modalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html',
            controller: ModalInstanceCtrl,
            resolve: {
              items: function () {
                return self.preduzeca;
              },
              type: function(){
            	  return "Preduzece";
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            self.preduzece = self.preduzeca[selectedItem];
          }, function () {
          });
        };
*/
        function showWorks() {

            var modalInstance = $uibModal.open({
              templateUrl: 'myModalContent.html',
              controller: ModalInstanceCtrl,
              resolve: {
                items: function () {
                  return self.delatnosti;
                },
                type: function(){
              	  return "Delatnost";
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
              self.delatnost = self.delatnosti[selectedItem];
            }, function () {
            });
          };

    function fetchAllPartnere(){
    	reset();
    	PoslovniPartnerService.fetchAllPartnere()
            .then(
            function(d) {
            	self.partneri = [];
                self.adrese = d["adrese"];
                self.preduzeca = d["preduzeca"];
                self.partneri = d["partneri"];
                self.delatnosti = d["delatnosti"];
                //$scope.indSelectedVote = 0;
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }
 
    function createPartnera(partner){
    	PoslovniPartnerService.createPartnera(partner)
            .then(
            fetchAllPartnere,
            function(errResponse){
            	alert(errResponse.data);
                console.error('Error while creating User');
            }
        );
    }
 
    function updatePartnera(partner, id){
    	PoslovniPartnerService.updatePartnera(partner, id)
            .then(
            fetchAllPartnere,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deletePartnera(id){
    	PoslovniPartnerService.deletePartnera(id)
            .then(
            fetchAllPartnere,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.adresa.id != null && !angular.isUndefined(self.adresa.id) && self.preduzece.id != null 
    				&& !angular.isUndefined(self.preduzece.id) && self.delatnost.id != null	
    				&& !angular.isUndefined(self.delatnost.id)){
    			self.partner.adresa = self.adresa;
	            self.partner.preduzece = self.preduzece;
	            self.partner.sifraDelatnosti = self.delatnost;
	            createPartnera(self.partner);
    		}
        }else if (self.currState === "edit"){
        	if(self.adresa.id != null && !angular.isUndefined(self.adresa.id) && self.preduzece.id != null 
    				&& !angular.isUndefined(self.preduzece.id) && self.delatnost.id != null	&& !angular.isUndefined(self.delatnost.id)){
        		self.partner.adresa = self.adresa;
	            self.partner.preduzece = self.preduzece;
	            self.partner.sifraDelatnosti = self.delatnost;
	            updatePartnera(self.partner, self.partner.id);
        	}
        } else if (self.currState === "search"){
        	self.partner.adresa = self.adresa;
            self.partner.preduzece = self.preduzece;
            self.partner.sifraDelatnosti = self.delatnost;
        	search(self.partner);
        }
    }
 
    function edit(id){
        for(var i = 0; i < self.partneri.length; i++){
            if(self.partneri[i].id === id) {
                self.partner = angular.copy(self.partneri[i]);
                self.adresa = self.partner.adresa;
                self.preduzece = self.partner.preduzece;
                self.delatnost = self.partner.sifraDelatnosti;
                break;
            }
        }
    }
 
    function remove(id){
        if(self.partner.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deletePartnera(id);
    }
 
 
    function reset(){
        self.partner={};
        self.adresa= {};
        //self.preduzece= {};
        self.delatnost={};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(partner){
    	PoslovniPartnerService.searchPartnere(partner)
        .then(
        function(d) {
        	self.partneri = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, type) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };
    	  $scope.type = type;

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);