'use strict';

angular.module('repo3App').controller('StanjeController', ['$scope','$uibModal', 'StanjeService', function($scope,$uibModal,StanjeService) {
    var self = this;
    self.stanje={};
    self.stanja=[];
    self.preduzece = {};
    self.racun = {};
    self.racuni = [];
    self.show = false;
    self.logged = false;
    self.children = ["StavkeIzvoda"];
    
    self.submit = submit;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showRacune = showRacune;

    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.popup1 = {
    	    opened: false
    };
    
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		if(args.parent != null){
    			self.stanje.racun = args.parent.racuni[args.parent.indSelected];
    			search(self.stanje);
    		} else {
    			fetchAllStanja();
    		}
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.stanje = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "add";
    		fetchAllStanja();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.stanja.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.stanja.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.stanja[indSelectedVote].id;
   		}
    };
   
    function showRacune() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.racuni;
            },
            type: function(){
          	  return "Racun";
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.racun = self.racuni[selectedItem];
        }, function () {
        });
      };
 
    function fetchAllStanja(){
    	StanjeService.fetchAllStanja()
            .then(
            function(d) {
            	self.stanja = [];
                self.stanja = d["stanja"];
                self.racuni = d["racuni"];
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }
 
    function createStanje(stanje){
    	StanjeService.createStanje(stanje)
            .then(
            fetchAllStanja,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.racun.id != null && !angular.isUndefined(self.racun.id)){
	            self.stanje.racun = self.racun;
	            createStanje(self.stanje);
    		}
        } else if (self.currState === "search"){
        	self.stanje.racun = self.racun;
        	search(self.stanje);
        }
	        reset();
    }
 
    function reset(){
        self.recun={};
        self.stanje= {};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(stanje){
    	StanjeService.searchStanja(stanje)
        .then(
        function(d) {
        	self.stanja = d;
        	self.stanje= {};
        	self.racun = {};
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, type) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };
    	  $scope.type = type;

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);