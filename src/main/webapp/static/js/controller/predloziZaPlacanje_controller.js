'use strict';
 
//angular.module('repo3App').controller('DrzavaController', ['$scope', 'DrzavaService', DRZCtrl]);
angular.module('repo3App').controller('PredlogController', PRDLCtrl);

function PRDLCtrl($scope,$filter, PredlogService) {
    var self = this;
    self.predlog={};
    self.predlozi=[];
    self.show = false;
    self.children = ['StaSePlaca', "NaloziZaPlacanje"];
    self.logged = false;
    self.status = ["na cekanju", "odobren", "odbijen"];
 
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.first = first;
    self.prev = prev;
    self.next = next;
    self.last = last;
    self.search = search;
    self.currState = "edit";
    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;

    $scope.popup1 = {
    	    opened: false
    };
    
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.predlog = {};
    	}
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllPredloge();
    	}
    });
    //TODO: change or scrap!!!
    $scope.$on('hidingYoCountries', function(event, args) {
    	self.show = args.arg[0];
    	
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selectedDrz = self.predlozi[$scope.indSelectedVote];
    			remove(selectedDrz.id);
    			self.currState = "edit";
    		}
    	}
    });
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		fetchAllPredloge();
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    function indexOfRole(propertyName,lookingForValue,array) {
        for (var i in array) {
            if (array[i][propertyName] == lookingForValue) {
                return i;
            }
        }
        return -1;
    }
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.predlozi.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.predlozi.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
    
    $scope.setSelected = function (indSelectedVote) {
    	$scope.indSelectedVote = indSelectedVote;
    	self.indSelected = indSelectedVote;
    	if(self.currState === "edit"){
    		var id = self.predlozi[indSelectedVote].id;
    		edit(id);
    	}
    };
    $scope.changeState = function(state){
    	if(self.currState != state){
    		self.currState = state;
    	}
    }
    
    function fetchAllPredloge(){
        PredlogService.fetchAllPredloge()
            .then(
            function(d) {
            	self.predlozi = d;
                $scope.indSelectedVote = -1;
                self.indSelected = -1;
                self.show = true;
                if (self.currState === "delete"){
                	self.currState = "edit";
                }
            },
            function(errResponse){
                console.error('Error while fetching Predlozi');
            }
        );
    }
 
    function createPredlog(predlog){
        PredlogService.createPredlog(predlog)
            .then(
            fetchAllPredloge,
            function(errResponse){
                console.error('Error while creating Predlog');
            }
        );
    }
 
    function updatePredlog(predlog, id){
        PredlogService.updatePredlog(predlog, id)
            .then(
            fetchAllPredloge,
            function(errResponse){
                console.error('Error while updating Predlog');
            }
        );
    }
 
    function deletePredlog(id){
        PredlogService.deletePredlog(id)
            .then(
            fetchAllPredloge,
            function(errResponse){
                console.error('Error while deleting Predlog');
            }
        );
    }
 
    function submit() {
        
    	if(self.currState === "add"){
            createPredlog(self.predlog);        	
        } else if(self.currState === "edit"){
            updatePredlog(self.predlog, self.predlog.id);
            console.log('Thing updated with id ', self.predlog.id);
        } else if (self.currState === "search"){
        	search(self.predlog);
        }
        reset();
    }
 
    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.predlozi.length; i++){
            if(self.predlozi[i].id === id) {
                self.predlog = angular.copy(self.predlozi[i]);
               // $scope.datum = new Date(self.predlog.datum);
                //self.predlog.datum = $filter('date')($scope.datum, 'dd-MM-yyyy');
                break;
            }
        }
    }
 
    function remove(id){
        console.log('id to be deleted', id);
        if(self.predlog.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deletePredlog(id);
    }
 
    function search(predlog){
    	PredlogService.searchPredloge(predlog)
        .then(
        function(d) {
        	self.predlozi = d;
            $scope.indSelectedVote = -1;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    function reset(){
        self.predlog={};
        $scope.myForm.$setPristine(); //reset Form
    }
};