'use strict';
 
angular.module('repo3App').controller('IzvestajiController', ['$scope', '$uibModal', 'IzvestajiService', 
                                                                    function($scope,$uibModal, IzvestajiService) {
	var self = this;
   
    self.partner={};
    self.show = false;
    self.dugmici = true;
    self.datumDonji ={};
    self.datumGornji = {};
    self.datumi = {};
    self.KarticaKupca = {};
    self.kupac = {};
    self.kupci = [];
    self.partneri = [];
    
    self.KUFShow = false;
    self.KIFShow = false;
    self.KKShow = false;
    self.IOSShow = false;

    self.logged = false;
    self.us = {};
    self.KarticaPartnera = KarticaPartnera;
    self.KUF = KUF;
    self.KIF = KIF;
    self.IOS = IOS;
    self.izvestaj = izvestaj;
   
    $scope.popup1 = {
    	    opened: false
    };
    
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    
    $scope.popup2 = {
    	    opened: false
    };
    
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };
    
    $scope.$on('dispAfterLog', function(event, args) {
    		fetchPartneri();
    		fetchKupce();
    });
    
    function fetchKUF(datumDonji){
    	IzvestajiService.fetchKUF(datumDonji)
            .then(
            function(d) {
            	
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchKUFPDF(parametar){
    	IzvestajiService.fetchKUFPDF(parametar)
            .then(
            function(d) {
            	
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchKIF(datum){
    	IzvestajiService.fetchKIF(datum)
            .then(
            function(d) {
            	
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchKarticePartnera(kupac){
    	IzvestajiService.fetchKarticePartnera(kupac)
            .then(
            function(d) {
            	
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchIOS(partner){
    	IzvestajiService.fetchIOS(partner)
            .then(
            function(d) {
            	
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchPartneri(){
    	IzvestajiService.fetchPartneri()
            .then(
            function(d) {
            	self.partneri = d;
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function fetchKupce(){
    	IzvestajiService.fetchKupce()
            .then(
            function(d) {
            	self.kupci = d;
            },
            function(errResponse){
                console.error('Error while fetching Ulazne Fakture');
            }
        );
    }
    
    function datumod(){
    	
    }
    
    function KarticaPartnera(){
    	self.dugmici = false;
    	self.KUFShow = true;
        self.KKShow = true;
        self.IOSShow = false;
    }
    
    function KUF(){
    	self.dugmici = false;
    	self.KUFShow = true;
        self.KKShow = false;
        self.IOSShow = false;
    }
    
    function KIF(){
    	self.dugmici = false;
    	self.KUFShow = true;
        self.KKShow = false;
        self.IOSShow = false;
        
    }
    function IOS(){
    	self.dugmici = false;
    	self.KUFShow = false;
        self.KKShow = false;
        self.IOSShow = true;
    }
    
    function izvestaj(){
    	if(self.KUFShow){
    		self.datumi.datumDonji = self.datumDonji;
        	self.datumi.datumGornji = self.datumGornji;
        	fetchKUF(self.datumi);
    	} else if(self.KIFShow){
    		self.datumi.datumDonji = self.datumDonji;
        	self.datumi.datumGornji = self.datumGornji;
        	fetchKIF(self.datumi);
    	} else if(self.KKShow){
    		self.KarticaKupca.datumDonji = self.datumDonji;
        	self.KarticaKupca.datumGornji = self.datumGornji;
        	self.KarticaKupca.kupac = self.kupac;
        	fetchKarticePartnera(self.KarticaKupca);
    	} else if(self.IOSShow){
        	fetchIOS(self.kupac);
    	}
    }
}]);
