'use strict';

angular.module('repo3App').controller('RacunPartneraController', ['$scope','$uibModal', 'RacunPartneraService', 
                                                                  function($scope,$uibModal,RacunPartneraService) {
    var self = this;
    self.banka={id:null};
    self.banke=[];
    self.poslovniPartner = {};
    self.poslovniPartneri = [];
    self.racun = {};
    self.racuni = [];
    self.show = false;
    self.logged = false;
    self.children = ["DnevnoStanje"];
    
    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.search = search;
    self.currState = "edit";
    self.showBanks = showBanks;
    self.showCorps = showCorps;

    self.indSelected = -1;
    $scope.indSelectedVote = -1;
    $scope.required = true;
    
    $scope.$on('dispAfterLog', function(event, args) {
    	if(self === args.ctrl){
    		self.logged = true;
    		self.show = true;
    		self.preduzece = args.info.preduzece;
    		if(args.parent != null){
    			self.racun.banka = args.parent.banke[args.parent.indSelected];
    			search(self.racun);
    		} else {
    			fetchAllRacune();
    		}
    	} else {
    		self.logged = false;
    		self.show = false;
    	}
    });
    
    $scope.$on('changeState', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = args.state;
    		$scope.required = (self.currState === "search") ? false : true;
    		self.racun = {};
    	}
    });
    $scope.$on('hide', function(event, args) {
    	self.show = false;
    	
    });
    $scope.$on('resetEvent', function(event, args) {    	
    	if(self === args.ctrl){
    		self.currState = "edit";
    		fetchAllRacune();
    	}
    });
    $scope.$on('changeSelection', function(event, args) {    	
    	if(self === args.ctrl){
    		switch (args.direction) {
	            case 'prev':
	                prev();
	                break;
	            case 'next':
	                next();
	                break;
	            case 'first':
	            	first();
	            	break;
	            case 'last':
	            	last();
	            	break;
    		}
    	}
    });
    
    $scope.$on('deleteSel', function(event, args) {    	
    	if(self === args.ctrl){
    		if($scope.indSelectedVote != -1){
    			var selected = self.racuni[$scope.indSelectedVote];
    			remove(selected.id);
    			self.currState = "edit";
    		}
    	}
    });
    
    function first(){
    	$scope.indSelectedVote = 0;
    	self.indSelected = $scope.indSelectedVote;
    }
    function prev(){
    	if(($scope.indSelectedVote != 0) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote - 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function next(){
    	if(($scope.indSelectedVote != self.racuni.length) && ($scope.indSelectedVote != -1)){
    		$scope.indSelectedVote = $scope.indSelectedVote + 1;
    		self.indSelected = $scope.indSelectedVote;
    	}
    }
    
    function last(){
    	$scope.indSelectedVote = self.racuni.length - 1;
    	self.indSelected = $scope.indSelectedVote;
    }
 
    $scope.indSelectedVote = null;
    $scope.setSelected = function (indSelectedVote) {
       $scope.indSelectedVote = indSelectedVote;
       self.indSelected = $scope.indSelectedVote;
       if(self.currState === "edit"){
    	   var id = self.racuni[indSelectedVote].id;
    	   edit(id);
   		}
    };
   
    function showBanks() {

        var modalInstance = $uibModal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            items: function () {
              return self.banke;
            },
            type: function(){
          	  return "Banku";
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          self.banka = self.banke[selectedItem];
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      function showCorps() {

          var modalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html',
            controller: ModalInstanceCtrl,
            resolve: {
              items: function () {
                return self.poslovniPartneri;
              },
              type: function(){
            	  return "Partnera";
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            self.poslovniPartner = self.poslovniPartneri[selectedItem];
          }, function () {
          });
        };
      
    function fetchAllRacune(){
    	RacunPartneraService.fetchAllRacune()
            .then(
            function(d) {
            	self.racuni = [];
                self.banke = d["banke"];
                self.poslovniPartneri = d["partneri"];
                self.racuni = d["racuni"];
                //$scope.indSelectedVote = 0;
            },
            function(errResponse){
                console.error('Error while fetching Receits');
            }
        );
    }
 
    function createRacun(racun){
    	RacunPartneraService.createRacun(racun)
            .then(
            fetchAllRacune,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
 
    function updateRacun(racun, id){
    	RacunPartneraService.updateRacun(racun, id)
            .then(
            fetchAllRacune,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }
 
    function deleteRacun(id){
    	RacunPartneraService.deleteRacun(id)
            .then(
            fetchAllRacune,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }
 
    function submit() {
    	if (self.currState === "add"){
    		if(self.banka.id != null && !angular.isUndefined(self.banka.id) && self.poslovniPartner.id != null 
    				&& !angular.isUndefined(self.poslovniPartner.id)){
	            self.racun.banka = self.banka;
	            self.racun.poslovniPartner = self.poslovniPartner;
	            createRacun(self.racun);
    		}
        }else if (self.currState === "edit"){
        	if(self.banka.id != null && !angular.isUndefined(self.banka.id) && self.poslovniPartner.id != null 
    				&& !angular.isUndefined(self.poslovniPartner.id)){
        		self.racun.banka = self.banka;
	            self.racun.poslovniPartner = self.poslovniPartner;
	            updateRacun(self.racun, self.racun.id);
        	}
        } else if (self.currState === "search"){
        	self.racun.banka = self.banka;
            self.racun.poslovniPartner = self.poslovniPartner;
        	search(self.racun);
        }
	        reset();
    }
 
    function edit(id){
        for(var i = 0; i < self.racuni.length; i++){
            if(self.racuni[i].id === id) {
                self.racun = angular.copy(self.racuni[i]);
                self.banka = self.racun.banka;
                self.poslovniPartner = self.racun.poslovniPartner;
                break;
            }
        }
    }
 
    function remove(id){
        if(self.racun.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteRacun(id);
    }
 
 
    function reset(){
        self.recun={};
        self.banka= {};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function search(racun){
    	RacunPartneraService.searchRacune(racun)
        .then(
        function(d) {
        	self.racuni = d;
            $scope.indSelectedVote = -1;
            self.indSelected = $scope.indSelectedVote;
        },
        function(errResponse){
            console.error('Error while fetching Users');
        }
        );
    }
    
    var ModalInstanceCtrl = function ($scope, $uibModalInstance, items, type) {

    	  $scope.items = items;
    	  $scope.selected = {
    	    item: -1
    	  };
    	  $scope.type = type;

    	  $scope.ok = function () {
    		  $uibModalInstance.close($scope.selected.item);
    	  };

    	  $scope.cancel = function () {
    		  $uibModalInstance.dismiss('cancel');
    	  };
    	};
}]);