<div class="generic-container" ng-controller="ZatvaranjeUFController as ctrl" id="ZatvaranjeUF">
      <style>
      .ScrollStyle
	{
    max-height: 250px;
    overflow-y: scroll;
	}

  	tr.selected td {
      background: #96a6ff;
    }
    
    tr.selectedOther1 td {
      background: #96a6ff;
    }
    
    div{
     font-size:small;
    }
    

    
    .inline{
    display: inline-block;
    }
    
    
    
      </style>
       <!-- DUGMICI ZA BIRANJE VRSTE ZATVARANJA -->    
      <div id="dugmici" class="centered" style="display: inline-block; align-self: center;  padding-top: 150px; padding-left: 400px;" ng-show="ctrl.dugmici" >
		 <button type="button" class="btn btn-w3r" ng-click="ctrl.showZatvaranje()">Rucno zatvaranje</button> 
		 <button type="button" class="btn btn-w3r" ng-click="ctrl.zatvoriAuto()">Automatsko zatvaranje</button> 
      </div>
      
      <!-- POSLOVNI PARTNERI  -->
      <div class="panel panel-default" ng-show="ctrl.stavkeIzvodashow">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Partnera </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                          	 
                              <th>Naziv</th>
                              <th>PIB</th>
                              <th>Maticni Broj</th>
                              <th>Broj Telefona</th>
                              <th>Fax</th>
                              <th>Email</th>
                              <th>Delatnost</th>
                              <th>Adresa</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="pp in ctrl.partneri" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                             
                              <td><span ng-bind="pp.naziv"></span></td>
                              <td><span ng-bind="pp.pib"></span></td>
                              <td><span ng-bind="pp.maticniBroj"></span></td>
                              <td><span ng-bind="pp.brTelefona"></span></td>
                              <td><span ng-bind="pp.fax"></span></td>
                              <td><span ng-bind="pp.email"></span></td>
                              <td><span ng-bind="pp.sifraDelatnosti.nazivDelatnosti"></span></td>
                              <td><span ng-bind="pp.adresa.ulica"></span><span ng-bind="pp.adresa.broj"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
    
      <!-- STAVKE IZVODAAA -->
      <div ng-show="ctrl.stavkeIzvodashow" style="max-width: 50%; float: left">
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista stavki izvoda </span></div>
              <div class="tablecontainer ScrollStyle"  >
                  <table class="table table-hover  " >
                      <thead>
                          <tr>
                              
                              <th>Duznik</th>
                              <th>Svrha placanja</th>
                              <th>Iznos</th>
                              <th>Primalac</th>
                              <th>Preostali iznos</th>
                              <th>Datum naloga</th>
                              <th>Datum valute</th>
                              <th>Racun primaoca</th>
                              <th>Racun duznika</th>
                              <th>Model zaduzenja</th>
                              <th>Poziv na broj zaduzenja</th>
                              <th>Model odobrenja</th>
                              <th>Poziv na broj odobrenja</th>
                              <th>Smer</th>
                              <th>Za Izvod</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="s in ctrl.stavke" ng-click="setSelectedS($index)" ng-class="{selected: $index === indSelectedVoteS}">
                              
                              <td><span ng-bind="s.duznik"></span></td>
                              <td><span ng-bind="s.svrhaPlacanja"></span></td>
                              <td><span ng-bind="s.iznos"></span></td>
                              <td><span ng-bind="s.primalac"></span></td>
                              <td><span ng-bind="s.preostaliIznos"></span></td>
                              <td><span ng-bind="s.datumNaloga | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.datumValute | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.racunPrimaoca"></span></td>
                              <td><span ng-bind="s.racunDuznika"></span></td>
                              <td><span ng-bind="s.modelZaduzenja"></span></td>
                              <td><span ng-bind="s.pozivNaBrojZaduzenja"></span></td>
                              <td><span ng-bind="s.modelOdobrenja"></span></td>
                              <td><span ng-bind="s.pozivNaBrojOdobrenja"></span></td>
                              <td><span>{{s.smer === 'P' ? 'U korist' : 'Na teret'}}</span></td>
                              <td><span ng-bind="s.dnevnoStanje.datumIzvoda.substr(8,2)"></span>.<span ng-bind="s.dnevnoStanje.datumIzvoda.substr(5,2)"></span>.<span ng-bind="s.dnevnoStanje.datumIzvoda.substr(0,4)"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          </div>
          
          <!-- ng-show="ctrl.ifaktureshow" -->
          <!-- IZLAZNE FAKTURE -->   
       <div id="izlazneFakture" ng-show="ctrl.ifaktureShow" style="max-width: 50%; float:right">
          <div class="panel panel-default">
                <!-- Default panel contents -->
                
                <div class="panel-heading"><span class="lead">Lista ulaznih faktura </span></div>
              <div class="tablecontainer ScrollStyle" >
                  <table class="table table-hover ">
                      <thead>
                          <tr>
                              <th>Broj fakture</th>
                              <th>Datum fakture</th>
                              <th>Datum valute</th>
                              <th>Vrednost robe</th>
                              <th>Vrednost usluga</th>
                              <th>Vrednost roba i usluga</th>
                              <th>Ukupan rabat</th>
                              <th>Ukupan porez</th>
                              <th>Oznaka valute</th>
                              <th>Ukupan iznos</th>
                              <th>Preostali iznos</th>
                              <th>Poslovna godina</th>
                              <th>Poslovni partner</th>
                              <th>Racun partnera</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="i in ctrl.ifakture" ng-click="setSelectedF($index)" ng-class="{selected: $index === indSelectedVoteF}">
                              <td><span ng-bind="i.brojFakture"></span></td>
                              <td><span ng-bind="i.datumFakture | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="i.datumValute | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="i.vrednostRobe"></span></td>
                              <td><span ng-bind="i.vrednostUsluga"></span></td>
                              <td><span ng-bind="i.ukupnoRobaIUsluge"></span></td>
                              <td><span ng-bind="i.ukupanRabat"></span></td>
                              <td><span ng-bind="i.ukupanPorez"></span></td>
                              <td><span ng-bind="i.oznakaValute"></span></td>
                              <td><span ng-bind="i.ukIznos"></span></td>
                              <td><span ng-bind="i.preostaliIznos"></span></td>
                              <td><span ng-bind="i.poslovnaGodina.godina"></span></td>
                              <td><span ng-bind="i.poslovniPartner.naziv"></span></td>
                              <td><span ng-bind="i.racunPartnera.brRacuna"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          </div>
        
       <div class="panel panel-default" ng-show="ctrl.ifaktureShow">
 
             <div class="row centered" style="margin: auto">
            		 <label>Ukupno u korist:</label>
             		 <input type="number" ng-model="ctrl.iznos" required="iznos" >
             		 <br>
             		 <label>Ukupno fakture:</label>
             		 <input type="number" ng-model="ctrl.iznos" required="iznos" >
             		 <br>
                     <button type="button" ng-click="ctrl.zatvori()" class="btn btn-info btn-sm" >Zatvori</button>
                     <label>{{ctrl.currState}}</label>
                 </div>
             </div>
 
          
          
           
         <div ng-show="ctrl.stornoIzlaznihShow">
         
         <!-- STORNO IZLAZNIH FAKTURA -->
      <div  >
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista stavki zatvorenih ulaznih faktura </span></div>
              <div class="tablecontainer ScrollStyle"  >
                  <table class="table table-hover ">
                      <thead>
                          <tr>
                              <th>Poslovna godina</th>
                              <th>Datum izvoda</th>
                              <th>Banka</th>
                              <th>Iznos</th>
                              <th>Svrha placanja</th>
                              <th>Datum naloga</th>
                              <th>Datum valute izvoda</th>
                              <th>Broj fakture</th>
                              <th>Datum fakutre</th>
                              <th>Datum valute fakture</th>
                              <th>Ukupan porez</th>
                              <th>Ukupan iznos</th>
                              <th>Preostali iznos</th>
                              <th>Poslovni partner</th>
                              <th>Dnevno stanje za dan</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="s in ctrl.zatvorene" ng-click="selectOther1(s)" ng-class="{'selectedOther1': s.selectedOther1}">
                              <td><span ng-bind="s.faktura.poslovnaGodina.godina"></span></td>
                              <td><span ng-bind="s.datumIzvoda | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.stavkaIzvoda.dnevnoStanje.racun.banka.naziv"></span></td>
                              <td><span ng-bind="s.iznos"></span></td>
                              <td><span ng-bind="s.stavkaIzvoda.svrhaPlacanja"></span></td>
                              <td><span ng-bind="s.stavkaIzvoda.datumNaloga | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.stavkaIzvoda.datumValute | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.faktura.brojFakture"></span></td>
                              <td><span ng-bind="s.faktura.datumFakture | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.faktura.datumValute | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.faktura.ukupanPorez"></span></td>
                              <td><span ng-bind="s.faktura.id"></span></td>
                              <td><span ng-bind="s.faktura.preostaliIznos"></span></td>
                              <td><span ng-bind="s.faktura.poslovniPartner.naziv"></span></td>
                              <td><span ng-bind="s.stavkaIzvoda.dnevnoStanje.datumIzvoda | date: 'dd.MM.yyyy'"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          </div>
    
          
          <div class="panel panel-default" >
          
                      <div class="row">
                              <button type="button" ng-click="getSelectedZaRazvezivanje()" class="btn btn-info btn-sm" >Razvezi</button>
                              
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
          </div>
          
          <!-- IZVESTAJ AUTOMATSKI ZATVORENIH FAKTURA -->
          <div ng-show="ctrl.report">
          
          </div>
          
          
</div>
