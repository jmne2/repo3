<div class="generic-container" id="StaSePlaca" ng-controller="StaSePlacaController as ctrl">
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite {{type}}</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
						<a ng-click="selected.item = $index" ng-if="type === 'Racun'">{{ item.brRacuna }}</a>
						<a ng-click="selected.item = $index" ng-if="type === 'Fakturu'">{{ item.brojFakture }} {{item.poslovnaGodina.godina}}</a>
						<a ng-click="selected.item = $index" ng-if="type === 'Predlog'">{{item.broj}} {{item.datum.substr(8,2)}}.{{item.datum.substr(5,2)}}.{{item.datum.substr(0,4)}}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
         <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista sta se placa </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Iznos</th>
                              <th>Sa kog racuna</th>
                              <th>Faktura</th>
                              <th>U predlogu placanja</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="pp in ctrl.placanje" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="pp.id"></span></td>
                              <td><span ng-bind="pp.iznos"></span></td>
                              <td><span ng-bind="pp.saKogRacuna"></span></td>
                              <td><span ng-bind="pp.faktura.brojFakture"></span> | <span ng-bind="pp.faktura.datumFakture | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="pp.predlogPlacanja.broj"></span> | <span ng-bind="pp.predlogPlacanja.datum | date: 'dd.MM.yyyy'"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.placam.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="iznos">Iznos</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.placam.iznos" id="iznos" class="naziv form-control input-sm" placeholder="Unesite iznos" ng-required="required" ng-minlength="1"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.iznos.$error.required">This is a required field</span>
                                      <span ng-show="myForm.iznos.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.iznos.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="adresa">Sa kog Racuna</label>
                              <button type="button" ng-click="ctrl.showRacune()" class="btn btn-info btn-sm">Izaberi Racun</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="adresa">{{ctrl.racun.brRacuna}}</label>
                              </div>
                          </div>
                      </div>  
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="faktura">Faktura</label>
                              <button type="button" ng-click="ctrl.showFakture()" class="btn btn-info btn-sm">Izaberi Fakturu</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="faktura">{{ctrl.faktura.brojFakture}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="predlog">Predlog Placanja</label>
                              <button type="button" ng-click="ctrl.showPredloge()" class="btn btn-info btn-sm">Izaberi Predlog</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="predlog">{{ctrl.predlog.broj}} <span ng-bind="ctrl.predlog.datum | date: 'dd.MM.yyyy'"></span></label>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          </div>
      </div>