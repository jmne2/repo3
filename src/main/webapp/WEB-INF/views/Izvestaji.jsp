<div class="generic-container" ng-controller="IzvestajiController as ctrl" id="Izvestaji">
      <style>
      .ScrollStyle
	{
    max-height: 250px;
    overflow-y: scroll;
	}

  	tr.selected td {
      background: #96a6ff;
    }
    
    tr.selectedOther td {
      background: #96a6ff;
    }
    
    div{
     font-size:small;
    }
    

    
    .inline{
    display: inline-block;
    }
    
      </style>
       <!-- DUGMICI ZA BIRANJE IZVESTAJA -->   
       <div id = "dugmici"> 
	      <div class="centered" style="display: inline-block; align-self: center;  padding-top: 25px; padding-left: 400px;" ng-show="ctrl.dugmici" >
			 <button type="button" class="btn btn-w3r" ng-click="ctrl.KUF()">1.	KUF  </button> 
			 <button type="button" class="btn btn-w3r" ng-click="ctrl.KIF()" target="_blank">2.	KIF </button>
	      </div>
	      
	       <div class="centered" style="display: inline-block; align-self: center;  padding-top: 10px; padding-left: 400px;" ng-show="ctrl.dugmici" >
			 <button type="button" class="btn btn-w3r" ng-click="ctrl.KarticaPartnera()">3.	Kartica partnera  </button> 
			 <button type="button" class="btn btn-w3r" ng-click="ctrl.IOS()">4.	IOS</button>  
	      </div>
      </div>
  
      <div ng-show="ctrl.KUFShow"  style="display: inline-block; align-self: center;  padding-top: 150px; padding-left: 400px;"> 
      
	      <label>Datum od: </label>
	      <input type="text"  ng-bind="ctrl.datumDonji | date: 'dd.MM.yyyy'" disabled = "true" />
	      
	      <div uib-datepicker-popup="dd-MM-yyyy" ng-model="ctrl.datumDonji" is-open="popup1.opened" close-text="Close" ></div>
	         <span class="input-group-btn">
	           <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
	     	 </span>
	                             
	       <label>Datum do: </label>
	       <input type="text"  ng-bind="ctrl.datumGornji | date: 'dd.MM.yyyy'" disabled = "true" />
	
	        <div uib-datepicker-popup="dd-MM-yyyy" ng-model="ctrl.datumGornji" is-open="popup2.opened" close-text="Close" ></div>
		        <span class="input-group-btn">
		          <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
		        </span>
		        
		        <div ng-show = "ctrl.KKShow">
		        <label>Odaberite kupca:</label>
		        <select ng-model="kupac" ng-options="item for item in ctrl.kupci">
				</select>
		     </div>
		     
		     <button type="button" class="btn btn-default" ng-click="ctrl.izvestaj()">OK</button>
	    </div>
	    
	    <div ng-show = "ctrl.IOSShow"  style="display: inline-block; align-self: center;  padding-top: 150px; padding-left: 400px;">
		     	<label>Odaberite poslovnog partnera:</label>
		        <select ng-model="kupac" ng-options="item for item in ctrl.partneri">
				</select>
		 </div>
        
                              <!-- 
					       <label>Datum do:</label>
					       <input type="text" class="form-control" uib-datepicker-popup="dd-MMMM-yyyy" ng-model="ctrl.datumGornji" ng-disabled="disableForm"/>
                              <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()" ><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
					           -->
    
</div>
<!-- @RequestMapping(value = "/b", method = RequestMethod.GET, produces = APPLICATION_PDF)
public @ResponseBody HttpEntity<byte[]> downloadB() throws IOException {
    File file = getFile();
    byte[] document = FileCopyUtils.copyToByteArray(file);
    HttpHeaders header = new HttpHeaders();
    header.setContentType(new MediaType("application", "pdf"));
    header.set("Content-Disposition", "inline; filename=" + file.getName());
    header.setContentLength(document.length);
    return new HttpEntity<byte[]>(document, header);
} -->