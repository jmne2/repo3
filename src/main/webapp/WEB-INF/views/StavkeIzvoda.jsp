<div class="generic-container" ng-controller="StavkeIzvodaController as ctrl" id="StavkeIzvoda">
       <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite Izvod</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
                    	<a ng-click="selected.item = $index">{{ item.datumIzvoda.substr(8,2)}}.{{item.datumIzvoda.substr(5,2)}}.{{item.datumIzvoda.substr(0,4)}} {{item.racun.brRacuna}}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
       <div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista stavki izvoda </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Duznik</th>
                              <th>Svrha placanja</th>
                              <th>Iznos</th>
                              <th>Primalac</th>
                              <th>Datum naloga</th>
                              <th>Datum valute</th>
                              <th>Racun primaoca</th>
                              <th>Racun duznika</th>
                              <th>Model zaduzenja</th>
                              <th>Poziv na proj zaduzenja</th>
                              <th>Model odobrenja</th>
                              <th>Poziv na broj odobrenja</th>
                              <th>Smer</th>
                              <th>Za Izvod</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="s in ctrl.stavke" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="s.id"></span></td>
                              <td><span ng-bind="s.duznik"></span></td>
                              <td><span ng-bind="s.svrhaPlacanja"></span></td>
                              <td><span ng-bind="s.iznos"></span></td>
                              <td><span ng-bind="s.primalac"></span></td>
                              <td><span ng-bind="s.datumNaloga | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.datumValute | date: 'dd.MM.yyyy'"></span></td>
                              <td><span ng-bind="s.racunPrimaoca"></span></td>
                              <td><span ng-bind="s.racunDuznika"></span></td>
                              <td><span ng-bind="s.modelZaduzenja"></span></td>
                              <td><span ng-bind="s.pozivNaBrojZaduzenja"></span></td>
                              <td><span ng-bind="s.modelOdobrenja"></span></td>
                              <td><span ng-bind="s.pozivNaBrojOdobrenja"></span></td>
                              <td><span>{{s.smer === 'P' ? 'U korist' : 'Na teret'}}</span></td>
                              <td><span ng-bind="s.dnevnoStanje.datumIzvoda.substr(8,2)"></span>.<span ng-bind="s.dnevnoStanje.datumIzvoda.substr(5,2)"></span>.<span ng-bind="s.dnevnoStanje.datumIzvoda.substr(0,4)"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.stavka.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="duznik">Duznik</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavka.duznik" id="duznik" class="oznaka form-control input-sm" placeholder="Unesite duznika" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.duznik.$error.required">This is a required field</span>
                                      <span ng-show="myForm.duznik.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.duznik.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="svrha">Svrha placanja</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavka.svrhaPlacanja" id="svrha" class="oznaka form-control input-sm" placeholder="Unesite svrhu placanja" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.svrha.$error.required">This is a required field</span>
                                      <span ng-show="myForm.svrha.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.svrha.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="iznos">Iznos</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.iznos" id="iznos" class="oznaka form-control input-sm" placeholder="Unesite iznos" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.iznos.$error.required">This is a required field</span>
                                      <span ng-show="myForm.iznos.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.iznos.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="primalac">Primalac</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.stavka.primalac" id="primalac" class="oznaka form-control input-sm" placeholder="Unesite primaoca" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.primalac.$error.required">This is a required field</span>
                                      <span ng-show="myForm.primalac.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.primalac.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datumNaloga">Datum Naloga</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" ng-model="ctrl.stavka.datumNaloga | date: 'dd.MM.yyyy'" placeholder="Unesite datum" disabled ng-required="required"  />
                              <div uib-datepicker-popup="dd-MM-yyyy" ng-model="ctrl.stavka.datumNaloga" is-open="popup1.opened" close-text="Close" ></div>
					          <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datumValute">Datum Valute</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" ng-model="ctrl.stavka.datumValute | date: 'dd.MM.yyyy'" placeholder="Unesite datum" disabled ng-required="required"  />
                              <div uib-datepicker-popup="dd-MM-yyyy" ng-model="ctrl.stavka.datumValute" is-open="popup2.opened" close-text="Close" ></div>
					          <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="rPrimaoca">Racun Primaoca</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.racunPrimaoca" string-to-number id="rPrimaoca" class="oznaka form-control input-sm" placeholder="Unesite racun primaoca" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 18}}" ng-maxlength="18"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.rPrimaoca.$error.required">This is a required field</span>
                                      <span ng-show="myForm.rPrimaoca.$error.minlength">Minimum length required is 18</span>
                                      <span ng-show="myForm.rPrimaoca.$error.maxlength">Maximum length required is 18</span>
                                      <span ng-show="myForm.rPrimaoca.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="rDuznika">Racun Duznika</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.racunDuznika" string-to-number id="rDuznika" class="oznaka form-control input-sm" placeholder="Unesite racun duznika" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 18}}" ng-maxlength="18"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.rDuznika.$error.required">This is a required field</span>
                                      <span ng-show="myForm.rDuznika.$error.minlength">Minimum length required is 18</span>
                                      <span ng-show="myForm.rDuznika.$error.maxlength">Maximum length required is 18</span>
                                      <span ng-show="myForm.rDuznika.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>                          
                     <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="modelZad">Model Zaduzenja</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.modelZaduzenja" string-to-number id="modelZad" class="oznaka form-control input-sm" placeholder="Unesite model zaduzenja" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.modelZad.$error.required">This is a required field</span>
                                      <span ng-show="myForm.modelZad.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.modelZad.$error.maxlength">Maximum length required is 5</span>
                                      <span ng-show="myForm.modelZad.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="pozivZad">Poziv na broj zaduzenja</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.pozivNaBrojZaduzenja" string-to-number id="pozivZad" class="oznaka form-control input-sm" placeholder="Unesite poziv na broj zaduzenja" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}" ng-maxlength="20"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.pozivZad.$error.required">This is a required field</span>
                                      <span ng-show="myForm.pozivZad.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.pozivZad.$error.maxlength">Maximum length required is 20</span>
                                      <span ng-show="myForm.pozivZad.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>                          
                       <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="modelOd">Model Odobrenja</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.modelOdobrenja" string-to-number id="modelOd" class="oznaka form-control input-sm" placeholder="Unesite model odobrenja" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}" ng-maxlength="5"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.modelOd.$error.required">This is a required field</span>
                                      <span ng-show="myForm.modelOd.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.modelOd.$error.maxlength">Maximum length required is 5</span>
                                      <span ng-show="myForm.modelOd.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="pozivOd">Poziv na broj odobrenja</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.stavka.pozivNaBrojOdobrenja" string-to-number id="pozivZad" class="oznaka form-control input-sm" placeholder="Unesite poziv na broj odobrenja" ng-required="required" ng-minlength="{{ctrl.currState === 'search' ? 0 : 1}}" ng-maxlength="20"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.pozivOd.$error.required">This is a required field</span>
                                      <span ng-show="myForm.pozivOd.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="myForm.pozivOd.$error.maxlength">Maximum length required is 20</span>
                                      <span ng-show="myForm.pozivOd.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div> 
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="maticna">Izvod</label>
                              <button type="button" ng-click="ctrl.showIzvode()" class="btn btn-info btn-sm">Izaberi Izvod</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="maticna">{{ctrl.stanje.datumIzvoda.substr(8,2)}}.{{ctrl.stanje.datumIzvoda.substr(5,2)}}.{{ctrl.stanje.datumIzvoda.substr(0,4)}}</label>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit" id ="subId" value="Commit" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Rollback</button>
                              <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
 
      </div>
      </div>
