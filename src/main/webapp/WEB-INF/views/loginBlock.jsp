<div class="generic-container">
<div class="panel panel-default" style="max-width:400px;max-height:500px;margin-left:auto; margin-right:auto;">
              <div class="formcontainer">
                  <form ng-submit="submitLog()" name="logForm" class="form-horizontal">
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="uName">Naziv</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="us.uname" id="uName" class="naziv form-control input-sm" placeholder="Unesite Korisnicko ime" required ng-minlength="1"/>
                                  <div class="has-error" ng-show="logForm.$dirty">
                                      <span ng-show="logForm.uName.$error.required">This is a required field</span>
                                      <span ng-show="logForm.uName.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="logForm.uName.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="pName">Lozinka</label>
                              <div class="col-md-7">
                                  <input type="password" ng-model="us.pname" id="pName" class="naziv form-control input-sm" placeholder="Unesite Lozinku" required ng-minlength="1"/>
                                  <div class="has-error" ng-show="logForm.$dirty">
                                      <span ng-show="logForm.pName.$error.required">This is a required field</span>
                                      <span ng-show="logForm.pName.$error.minlength">Minimum length required is 1</span>
                                      <span ng-show="logForm.pName.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-actions">
                              <input type="submit"  id ="subId" value="Uloguj se" class="btn btn-primary btn-sm" ng-disabled="logForm.$invalid">
                          </div>
                      </div>
                  </form>
              </div>
 </div>
 </div>