<div class="generic-container" id="DnevnaStanja" ng-controller="StanjeController as ctrl">
          <script type="text/ng-template" id="myModalContent.html">
        	<div class="modal-header">
            	<h3>Izaberite {{type}}</h3>
        	</div>
        	<div class="modal-body">
            	<ul>
                	<li ng-repeat="item in items">
                    	<a ng-click="selected.item = $index">{{ item.brRacuna }}</a>
                	</li>
            	</ul>
            	Selected: <b>{{ selected.item }} </b>
        	</div>
        		<div class="modal-footer">
            	<button class="btn btn-primary" ng-click="ok()">OK</button>
            	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        	</div>
    	</script>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">Lista Dnevnih Stanja </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Datum</th>
                              <th>Prethodno stanje</th>
                              <th>U korist</th>
                              <th>Na teret</th>
                              <th>Novo Stanje</th>
                              <th>Rezervisano</th>
                              <th>Za Racun</th>
                          </tr>
                      </thead>
                      <tbody>
                      <!-- ng-click="setSelected(d.id)" ng-class="{selected: d.id === idSelectedVote}" -->
                          <tr ng-repeat="r in ctrl.stanja" ng-click="setSelected($index)" ng-class="{selected: $index === indSelectedVote}">
                              <td><span ng-bind="r.id"></span></td>
                              <td><span ng-bind="r.datumIzvoda.substr(8,2)"></span>.<span ng-bind="r.datumIzvoda.substr(5,2)"></span>.<span ng-bind="r.datumIzvoda.substr(0,4)"></span></td>
                              <td><span ng-bind="r.prethodnoStanje"></span></td>
                              <td><span ng-bind="r.stanjeUKorist"></span></td>
                              <td><span ng-bind="r.stanjeNaTeret"></span></td>
                              <td><span ng-bind="r.novoStanje"></span></td>
                              <td><span ng-bind="r.rezervisano"></span></td>
                              <td><span ng-bind="r.racun.brRacuna"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="panel panel-default">
              <div class="formcontainer">
              	<h3>Zahtev novog dnevnog stanja</h3>
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.stanje.id" />
 					<div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="banka">Racun</label>
                              <button type="button" ng-click="ctrl.showRacune()" class="btn btn-info btn-sm">Izaberi Racun</button>
                              <div class="col-md-7">
                              <label class="col-md-2 control-lable" id="banka">{{ctrl.racun.brRacuna}}</label>
                              </div>
                          </div>
                      </div>
                       <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="datum">Datum</label>
                              <div class="col-md-7">
                              <input type="text" class="form-control" ng-model="ctrl.stanje.datumIzvoda | date: 'dd.MM.yyyy'" placeholder="Unesite datum" disabled ng-required="required"  />
                              <div uib-datepicker-popup="dd-MM-yyyy" ng-model="ctrl.stanje.datumIzvoda" is-open="popup1.opened" close-text="Close" ></div>
					          <span class="input-group-btn">
					            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
					          </span>
                              </div>
                          </div>
                      </div>
 					
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  id ="subId" value="Zatrazi novo dnevno stanje" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                               <label>{{ctrl.currState}}</label>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

   </div>
